/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package fr.miage.aai.parameters;

/**
 *
 * @author castagnos
 */
public interface Status {
    public static final String FAIL = "fail";
    public static final String SUCCESS = "success";
    public static final String WAIT = "wait";
    public static final String ACCEPT = "accept";
    public static final String DATE = "date";
    public static final int DEFAULT_WAIT = 10000;
    public static final int FAILURE_STATUS = 0;
    public static final int SUCCESS_STATUS = 1;
}
