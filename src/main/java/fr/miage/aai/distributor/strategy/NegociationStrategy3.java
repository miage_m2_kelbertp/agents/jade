package fr.miage.aai.distributor.strategy;

import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Negociation;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public class NegociationStrategy3 extends Negociation {
    @Override
    public PriceProposalNegociationModel negociate(PriceProposalNegociationModel ppnm, int step, Distributor distributor) {
        double prixPropose = ppnm.getPrice();
        boolean exclu = ppnm.getExclu();

        // Utilisation de seuil de prix à ne pas dépasser
        double seuilBas = (exclu) ? 1.2 * prixPropose : 1.1 * prixPropose;
        double seuilHaut = (exclu) ? 1.3 * prixPropose : 1.2 * prixPropose;
        double offre = 0;

        // Prix proposé inférieur au seuil bas ? On propose donc le seuil bas
        if (prixPropose < seuilBas) {
            offre = seuilBas;
        } else if (prixPropose > seuilHaut) {
            // Prix proposé superieur au seuil haut ? On propose le seuil haut
            offre = seuilHaut;
        } else {
            // Si c'est entre nos seuil, on propose le prix
            offre = prixPropose;
        }
        if (distributor.getTresorerie() < offre) {
            return ppnm.setPrice(-1);
        } else {
            if (step < 10) return ppnm.setPrice(offre);
            else return ppnm.setPrice(-1);
        }
    }
}