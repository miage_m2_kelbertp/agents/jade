package fr.miage.aai.distributor.strategy;

import fr.miage.aai.distributor.model.Distributor;

import java.time.LocalDate;
import java.util.Random;

public class SalesStrategy {
    public static void defineSalesPeriod(LocalDate ld, Distributor distributor) {
        if(ld.getDayOfYear() == 1) {
            distributor.setNbDaySales(20).setSales(false);
        }

        Random random = new Random();
        int rand = random.nextInt(10);
        if(rand == 1 && distributor.getNbDaySales() > 0) {
            rand = random.nextInt(5);
            double percent = (double) rand / 10;
            distributor.setSales(true).setNbDaySales(distributor.getNbDaySales() - 1).setPercentSales(percent);
        }
    }
}
