package fr.miage.aai.distributor.strategy;

import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;

import java.util.HashMap;

public class PriceAbonnementStrategy {

    public static void majPriceAbonnement(Distributor distributor) {

        // récupérer tous les films du catalogue
        HashMap<String, Film> cat = distributor.getCatalogue();

        // pour tous les films du catalogue additionner les prix
        double sum = 0;
        int nbFilm = 0;
        for (Film f : cat.values()) {
            nbFilm++;
            sum += f.getPriceBuy();
        }

        if (nbFilm == 0) {
            nbFilm = 1;
        }

        // calculer la moyenne
        double moy = sum / nbFilm;

        distributor.setPriceAbo(Math.max(moy * 0.001, 10.0));

    }
}
