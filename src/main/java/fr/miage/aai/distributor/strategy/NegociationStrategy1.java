package fr.miage.aai.distributor.strategy;

import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Negociation;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public class NegociationStrategy1 extends Negociation {
    @Override
    public PriceProposalNegociationModel negociate(PriceProposalNegociationModel ppnm, int step, Distributor distributor) {
        // Stratégie avec une offre minimal (entre 80% & 90% moi chère)
        // Offre maximal (entre 20% de + & 10% de plus)
        // On fait une moyenne des deux offres et on fait la proposition
        double prixPropose = ppnm.getPrice();
        boolean exclu = ppnm.getExclu();

        double offreMinimale = (exclu) ? 0.8 * prixPropose : 0.9 * prixPropose;
        double offreMaximale = (exclu) ? 1.2 * prixPropose : 1.1 * prixPropose;
        double offre = (offreMinimale + offreMaximale) / 2;
        if (distributor.getTresorerie() < offre) {
            return ppnm.setPrice(-1);
        } else {
            if (step < 10) return ppnm.setPrice(offre);
            else return ppnm.setPrice(-1);
        }
    }
}