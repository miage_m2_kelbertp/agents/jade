package fr.miage.aai.distributor.strategy;

import java.util.Random;

public class EnchereStrategy {
    public static double evaluateAction(double min, double max, boolean exclu) {
        Random random = new Random();

        // 35% of chance to refuse the proposal
        if (random.nextInt(100) < 35) {
            return -1;
        }

        int rand;
        if (exclu) {
            rand = random.nextInt(40 - 20) + 20;
        } else {
            rand = random.nextInt(50) - 20;
        }

        double percent = (double) rand / 100;
        double price = min + percent * min;

        for (int i = 0; i < 5; i++) {
            if (price < max) {
                return price;
            }
            rand = random.nextInt(30);
            percent = (double) rand / 100;
            price = min + percent * min;
        }
        return -1;
    }
}
