package fr.miage.aai.distributor.strategy;

import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Negociation;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public class NegociationStrategy2 extends Negociation {
    @Override
    public PriceProposalNegociationModel negociate(PriceProposalNegociationModel ppnm, int step, Distributor distributor) {
        double prixPropose = ppnm.getPrice();
        boolean exclu = ppnm.getExclu();
        double supplement = 0;
        double reduction = 0;
        // C'est une exclu ?
        if (exclu) {
            // Oui, donc on peut ajouter un supplément de prix car on souhaite le film
            supplement = (Math.random() * (0.1 - 0.05) + 0.05) * prixPropose;
        } else {
            // Non, donc on essaie de baisser le prix
            reduction = (Math.random() * (0.15 - 0.05) + 0.05) * prixPropose;
        }
        double offre = prixPropose + supplement - reduction;
        if (distributor.getTresorerie() < offre) {
            return ppnm.setPrice(-1);
        } else {
            if (step < 10) return ppnm.setPrice(offre);
            else return ppnm.setPrice(-1);
        }
    }
}