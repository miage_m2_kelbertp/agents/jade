package fr.miage.aai.distributor.agent;

import fr.miage.aai.distributor.behaviour.*;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.strategy.PriceAbonnementStrategy;
import fr.miage.aai.distributor.strategy.SalesStrategy;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.util.leap.Iterator;

import java.time.LocalDate;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static fr.miage.aai.utils.Register.registerService;

public class DistributorAgent extends Agent {
    public static final String SERVICE = "distributor";
    protected Distributor distributor;
    private LocalDate localDate = LocalDate.now().minusDays(1);

    protected void setup() {
        distributor = new Distributor(getLocalName());

        super.addBehaviour(new TickerBehaviour(this, 1000) {
            @Override
            protected void onTick() {
                DFAgentDescription dfd = new DFAgentDescription();
                DFAgentDescription[] result;
                try {
                    result = DFService.search(super.myAgent, dfd);
                    for (DFAgentDescription agentDescription : result) {
                        Iterator iter = agentDescription.getAllServices();
                        while (iter.hasNext()) {
                            ServiceDescription sd = (ServiceDescription) iter.next();

                            if (sd.getName().equals("ticker")) {
                                ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
                                message.addReceiver(agentDescription.getName());
                                message.setContent("What time is it ? Distributor");
                                super.myAgent.send(message);
//                                ACLMessage answer = super.myAgent.blockingReceive();
//                                LocalDate ld = (LocalDate) answer.getContentObject();
//                                if(localDate.getDayOfMonth() != ld.getDayOfMonth()) {
//                                }
                                ACLMessage answer = super.myAgent.blockingReceive();
                                try {
                                    LocalDate ld = (LocalDate) answer.getContentObject();
                                    if (localDate.getDayOfMonth() != ld.getDayOfMonth()) {
                                        localDate = ld;
                                        verifyNbAboFake(ld);
                                        verifySalesPeriod(ld);
                                        verifyMonthlyPaymentAbo(ld);

                                        initiateNegociation();
                                        // add behaviour FilmNegociationInitiator
                                        majPriceAbonnement();

                                        // Update tresorery for the day
                                        distributor.setTresorerie(distributor.getTresorerie() + 10000);


                                    }
                                } catch (UnreadableException ignored) {
                                }
                            }
                        }
                    }
                } catch (FIPAException e) {
                    Logger.getLogger(TickerBehaviour.class.getName()).log(Level.SEVERE, null, e);
                }

            }
        });
/*
        String arg1 = (String) getArguments()[0];
        String name = getName().split("@")[0];

        if (arg1 != null && arg1.equals("Test1Behaviour")) {
            System.out.println(name + " : Test1Behaviour");
            addBehaviour(new NegociationStrategy1(this));
        }

        if (arg1 != null && arg1.equals("Test2Behaviour")) {
            System.out.println(name + " : Test2Behaviour");
            addBehaviour(new EnchereStrategy2());
        }
*/
        addBehaviour(new FilmResponder(this));
        addBehaviour(new FilmNegociationResponder(this, MessageTemplate.MatchConversationId("Negociation")));

        addBehaviour(new SearchByAGResponder(this, MessageTemplate.MatchConversationId("FilmByAG")));
        addBehaviour(new SearchByFilmResponder(this, MessageTemplate.MatchConversationId("FilmByTitre")));

        addBehaviour(new SubscriptionResponder(this, MessageTemplate.MatchConversationId("Subscribe")));
        addBehaviour(new RatingResponder(this, MessageTemplate.MatchConversationId("Rating")));

        registerService(SERVICE, this);
    }

    private void initiateNegociation() {
        Random random = new Random();
        int rand = random.nextInt(3);
        if (rand == 0) {
            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
            //                          msg.addReceiver(agentDescription.getName());
            //                          msg.setContent("What time is it ? Distributor");
            //                          msg.setConversationId("film");
            //                          msg.setReplyWith("film" + System.currentTimeMillis());
            this.addBehaviour(new FilmNegociationInitiator(this, msg));
        }
    }

    private void verifyNbAboFake(LocalDate ld) {
        Random random = new Random();
        int rand = random.nextInt(2000);
        double salesValue = 1 - distributor.getPercentSales();
        double prorataAboValue = (distributor.getPriceAbo() / ld.lengthOfMonth()) * (ld.lengthOfMonth() - ld.getDayOfMonth());
        distributor.setNbAbo(distributor.getNbAbo() + rand).setTresorerie(distributor.getTresorerie() + rand * (prorataAboValue * salesValue));
    }

    private void verifySalesPeriod(LocalDate ld) {
        SalesStrategy.defineSalesPeriod(ld, distributor);
    }

    private void verifyMonthlyPaymentAbo(LocalDate ld) {

        //if 1 of month
        if(ld.getDayOfMonth() == 1) {
            double valueOfSales = 1 - distributor.getPercentSales();
            distributor.setTresorerie(distributor.getTresorerie() + distributor.getNbAbo() * (distributor.getPriceAbo() * valueOfSales));
        }
    }

    private void majPriceAbonnement() {
        PriceAbonnementStrategy.majPriceAbonnement(distributor);
    }


    public Distributor getDistributor() {
        return distributor;
    }

}
