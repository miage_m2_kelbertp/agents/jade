// https://github.com/mihaimaruseac/JADE-ARIA/blob/master/src/examples/protocols/ContractNetResponderAgent.java

package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;
import fr.miage.aai.distributor.strategy.EnchereStrategy;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.Agent;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetResponder;

import static fr.miage.aai.utils.Printer.print;
import static fr.miage.aai.utils.Serializer.serialize;

public class FilmResponder extends ContractNetResponder {
    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();
    public FilmResponder(Agent agent) {
        super(agent, MessageTemplate.and(
                MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
                MessageTemplate.MatchPerformative(ACLMessage.CFP)));
    }

    @Override
    protected ACLMessage handleCfp(ACLMessage cfp) throws RefuseException {
        if (distributor.isFull()) {
            throw new RefuseException("not-available :" + this.distributor.getName() + " is full");
        }

        // Deserialize the message
        PriceProposalNegociationModel ppnm = (PriceProposalNegociationModel) Serializer.deserialize(cfp.getContent(), PriceProposalNegociationModel.class);

        // If the film is already in the distributor's catalog, refuse the negociation
        if (distributor.haveFilm(ppnm.getFilm())) {
            throw new RefuseException("not-available :" + this.distributor.getName() + " already have this film");
        }

        double proposalMin = ppnm.getPrice();
        // If the tresorier is not available, refuse the negociation
        if (distributor.getTresorerie() < proposalMin) {
            throw new RefuseException("not-available :" + this.distributor.getName() + " has not enough money");
        }

        double proposal = EnchereStrategy.evaluateAction(proposalMin, distributor.getTresorerie(), ppnm.getExclu());
        if (proposal > 0) {
            // We provide a proposal
            print("Propose " + proposal + "€", "distributor", getLocalName());
            ACLMessage propose = cfp.createReply();
            propose.setPerformative(ACLMessage.PROPOSE);
            propose.setContent(serialize(ppnm.setPrice(proposal)));
            return propose;
        } else {
            // We refuse to provide a proposal
            print("Refuse", "distributor", getLocalName());
            refuseAndAddToBacklist(ppnm, cfp.getSender().getLocalName());
            throw new RefuseException("evaluation-failed");
        }
    }

    private String getLocalName() {
        return myAgent.getLocalName().split("@")[0];
    }

    @Override
    protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) {
        // Deserialize the message
        PriceProposalNegociationModel ppnm = (PriceProposalNegociationModel) Serializer.deserialize(cfp.getContent(), PriceProposalNegociationModel.class);

        // Maj Agent's tresoserie
        distributor.setTresorerie(distributor.getTresorerie() - ppnm.getPrice());
        distributor.addFilmToCatalog(new Film(ppnm.getFilm(), ppnm.getPrice(), ppnm.getExclu(), cfp.getSender().getLocalName()));

        print("A ajouté \"" + ppnm.getFilm() + "\" dans sa bibliothèque 🎉", "distributor", getLocalName());

        ACLMessage inform = accept.createReply();
        inform.setPerformative(ACLMessage.INFORM);

        return inform;
    }

    protected void handleRejectProposal(ACLMessage cfp, ACLMessage propose, ACLMessage reject) {
        PriceProposalNegociationModel ppnm = (PriceProposalNegociationModel) Serializer.deserialize(cfp.getContent(), PriceProposalNegociationModel.class);
        refuseAndAddToBacklist(ppnm, cfp.getSender().getLocalName());
    }

    private void refuseAndAddToBacklist(PriceProposalNegociationModel ppnm, String sender) {
        if (!distributor.haveFilmInBacklist(ppnm.getFilm())) {
            distributor.addFilmToBackList(new Film(ppnm.getFilm(), sender));
            print("A ajouté \"" + ppnm.getFilm() + "\" dans sa liste de films non acquis 📕", "distributor", getLocalName());
        }
    }
}
