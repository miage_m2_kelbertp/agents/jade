package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;
import fr.miage.aai.distributor.model.Negociation;
import fr.miage.aai.distributor.strategy.NegociationStrategy1;
import fr.miage.aai.distributor.strategy.NegociationStrategy2;
import fr.miage.aai.distributor.strategy.NegociationStrategy3;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.HashMap;
import java.util.Random;

import static fr.miage.aai.utils.Printer.print;

public class FilmNegociationResponder extends CyclicBehaviour {
    static HashMap<AID, Negociation> dataStores = new HashMap<>();

    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();

    MessageTemplate modele;

    public FilmNegociationResponder(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINE MODEL
            PriceProposalNegociationModel ppnm = null;
            try {
                ppnm = (PriceProposalNegociationModel) Serializer.deserialize(msg.getContent(), PriceProposalNegociationModel.class);
                if (ppnm == null) {
                    dataStores.remove(msg.getSender());
                    return;
                }
            } catch (Exception ignored) {
                dataStores.remove(msg.getSender());
                return;
            }


            Negociation negociation;

            // WORK WITH MULTI-AGENT AND ALIMENTATION DONNES
            if (!dataStores.containsKey(msg.getSender())) {
                Random rand = new Random();
                int randInt = rand.nextInt(3) + 1;
                negociation = switch (randInt) {
                    case 1 -> new NegociationStrategy1();
                    case 2 -> new NegociationStrategy2();
                    case 3 -> new NegociationStrategy3();
                    default -> throw new IllegalStateException("Unexpected value: " + rand);
                };
                dataStores.put(msg.getSender(), negociation);
                negociation.film = ppnm.getFilm();
                negociation.exclusive = ppnm.getExclu();
            }

            negociation = dataStores.get(msg.getSender());
            negociation.offreAutre = ppnm.getPrice();
            Negociation d = dataStores.get(msg.getSender());

            // DEFINE IF AGREE, REFUSE OR FAILURE
            switch (msg.getPerformative()) {
                case ACLMessage.AGREE -> {
                    d.accord = true;
                }
                case ACLMessage.REFUSE, ACLMessage.FAILURE -> d.rejet = true;
            }

            // GET OFFER FROM OTHER AGENT
            d.offreAutre = ppnm.getPrice();

            // IF NO AGREEMENT AND NO REJECTION
            if (!d.accord && !d.rejet) {
                d.step++;
                ppnm.setPrice(d.offreAutre);
                print("J'ai reçu une offre à %.2f".formatted(d.offreAutre), "distributor", myAgent.getLocalName());

                if (d.offreAutre <= d.offrePrecedente) {
                    d.accord = true;
                }

                if (!d.accord && !d.rejet) {
                    ppnm = negociation.negociate(ppnm, d.step, distributor);
                    d.offrePrecedente = ppnm.getPrice();

                    // SEND OFFER TO OTHER AGENT
                    var response = msg.createReply();
                    response.setContent(Serializer.serialize(ppnm));
                    myAgent.send(response);
                    print(" : J'ai envoyé une offre à %.2f".formatted(ppnm.getPrice()), "distributor", myAgent.getLocalName());
                }
            }

            // IF AGREEMENT
            if (d.accord) {
                var reponse = msg.createReply();
                reponse.setContent(String.valueOf(d.offreAutre));
                reponse.setPerformative(ACLMessage.AGREE);
                dataStores.remove(msg.getSender());
                print("J'envoie mon accord", "distributor", myAgent.getLocalName());
                myAgent.send(reponse);

                distributor
                        .setTresorerie(distributor.getTresorerie() - ppnm.getPrice())
                        .addFilmToCatalog(new Film(ppnm.getFilm(), msg.getSender().getLocalName(), ppnm.getPrice()));
                return;
            }

            // IF REJECTION
            if(d.rejet){
                var reponse = msg.createReply();
                reponse.setPerformative(ACLMessage.REFUSE);
                dataStores.remove(msg.getSender());
                print("J'envoie mon refus", "distributor", myAgent.getLocalName());
                myAgent.send(reponse);
                return;
            }

            // IF STEP OVER 10
            if(d.step == 10){
                var reponse = msg.createReply();
                reponse.setPerformative(ACLMessage.FAILURE);
                dataStores.remove(msg.getSender());
                print("J'envoie mon refus, trop de tours" + myAgent.getLocalName(), "distributor", myAgent.getLocalName());
                myAgent.send(reponse);
            }
        }
        else block();
    }
}
