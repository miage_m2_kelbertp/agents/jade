package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.*;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.HashMap;

import static fr.miage.aai.utils.Printer.print;

public class RatingResponder extends CyclicBehaviour {

    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();
    MessageTemplate modele;

    public RatingResponder(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINE MODEL
            ReturnMovieRating rmr = (ReturnMovieRating) Serializer.deserialize(msg.getContent(), ReturnMovieRating.class);
            if (rmr == null) {

                return;
            }


            if (msg.getPerformative() == ACLMessage.INFORM) {

                HashMap<String, Film> cat = distributor.getCatalogue();
                HashMap<String, Film> back = distributor.getBacklist();

                // pour tout film du cat
                for (Film f : cat.values()) {
                    if (f.getName().equals(rmr.getName())) {
                        f.setFamous(rmr.getRating());
                    }
                }

                // pour tout film du back
                for (Film f : back.values()) {
                    if (f.getName().equals(rmr.getName())) {
                        f.setFamous(rmr.getRating());
                    }
                }
            }

        }



        else

    block();
}

}
