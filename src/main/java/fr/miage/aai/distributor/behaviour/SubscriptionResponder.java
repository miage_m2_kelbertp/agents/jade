package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.SubscriptionTransaction;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import static fr.miage.aai.utils.Printer.print;

public class SubscriptionResponder extends CyclicBehaviour {

    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();
    MessageTemplate modele;
    public SubscriptionResponder(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINE MODEL
            SubscriptionTransaction ppnm = (SubscriptionTransaction) Serializer.deserialize(msg.getContent(), SubscriptionTransaction.class);
            if (ppnm == null) {

                return;
            }

            if (ppnm.getType() == SubscriptionTransaction.Type.subscribe) {
                distributor.setTresorerie(1500 * distributor.getPriceAbo() * (1 - distributor.getPercentSales()));
                distributor.setNbAbo(distributor.getNbAbo() + 1500);

                print("Nouveaux abonnements pris en compte", "distributor", myAgent.getLocalName());
            }

            if (ppnm.getType() == SubscriptionTransaction.Type.unsubscribe) {
                distributor.setNbAbo(distributor.getNbAbo() - 500);

                print("Nouveaux désabonnements pris en compte", "distributor", myAgent.getLocalName());
            }

            if (ppnm.getType() == SubscriptionTransaction.Type.askPrice) {

                var reponse = msg.createReply();
                reponse.setConversationId("Subscribe");
                SubscriptionTransaction obj = new SubscriptionTransaction(SubscriptionTransaction.Type.proposePrice, distributor.getPriceAbo() * (1 - distributor.getPercentSales()));
                reponse.setPerformative(ACLMessage.PROPOSE);
                if (obj.getPrice().isNaN()) {
                    System.out.println(distributor.getPriceAbo() * (1 - distributor.getPercentSales()));
                    System.out.println("");
                }
                reponse.setContent(Serializer.serialize(obj));
                reponse.setInReplyTo(msg.getReplyWith());
                reponse.setReplyWith(msg.getReplyWith().substring(0, msg.getReplyWith().length() - 1) + "1");
                reponse.setConversationId(msg.getConversationId());
                print("Nouveaux abonnements pris en compte", "distributor", myAgent.getLocalName());
                myAgent.send(reponse);
            }

        }
        else block();
    }

}
