package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;

import static fr.miage.aai.utils.Printer.print;

public class FilmNegociationInitiator extends OneShotBehaviour {
    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();

    public FilmNegociationInitiator(Agent a, ACLMessage msg) {
        super(a);
    }

    @Override
    public void action() {
        if (distributor.getBacklist().size() == 0 || distributor.isFull()) {
            return;
        }
        Film film = getRandomFilmFromBacklist();

        var msg = new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(new AID(film.getProducer(), AID.ISLOCALNAME));
        msg.setContent(Serializer.serialize(new PriceProposalNegociationModel(film.getName(), 0, false)));
        msg.setConversationId("Negociation");
        print("J'envoie une offre à 0€", "distributor", myAgent.getLocalName());
        myAgent.send(msg);
    }


    private Film getRandomFilmFromBacklist() {
        int index = (int) (Math.random() * distributor.getBacklist().size());
        ArrayList<Film> films = new ArrayList<>(distributor.getBacklist().values());
        return films.get(index);
    }
}
