package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.ReturnFilms;
import fr.miage.aai.utils.models.SearchFilmByAGModel;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;

import static fr.miage.aai.utils.Printer.print;

public class SearchByAGResponder extends CyclicBehaviour {

    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();
    MessageTemplate modele;
    public SearchByAGResponder(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINE MODEL
            SearchFilmByAGModel ppnm = (SearchFilmByAGModel) Serializer.deserialize(msg.getContent(), SearchFilmByAGModel.class);
            if (ppnm == null) {

                return;
            }

            var reponse = msg.createReply();
            reponse.setConversationId("FilmByAG");

            String genre = ppnm.getGenre();
            String actor = ppnm.getActor();

            ArrayList<String> films = distributor.searchFilmsByActorAndGenre(genre, actor);

            if(!films.isEmpty()){
                ReturnFilms infoFilms = new ReturnFilms(films);

                reponse.setPerformative(ACLMessage.INFORM);
                reponse.setContent(Serializer.serialize(infoFilms));
                reponse.setConversationId(msg.getConversationId());
                reponse.setReplyWith(msg.getReplyWith());
                reponse.setInReplyTo(msg.getReplyWith());
                print("Films trouvés", "distributor", myAgent.getLocalName());
                myAgent.send(reponse);
            }
            reponse.setPerformative(ACLMessage.REFUSE);
            reponse.setReplyWith(msg.getReplyWith());
            myAgent.send(reponse);

        }
        else block();
    }

    private void searchFilm(String titre) {
        distributor.haveFilmInCatalogue(titre);
    }
}
