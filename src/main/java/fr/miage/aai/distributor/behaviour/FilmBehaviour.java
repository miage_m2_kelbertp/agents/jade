package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.database.DatabaseConnexion;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmBehaviour extends OneShotBehaviour {

    public FilmBehaviour(Agent agent) {
        super(agent);
    }

    @Override
    public void action() {
        Connection con = DatabaseConnexion.getInstance();

        ResultSet rs = null;
        try {
            PreparedStatement preparedStatement = con.prepareStatement("""
                        select * from Film where titre like ?
                    """);
            preparedStatement.setString(1, "%" + myAgent.getArguments()[0] + "%");
            rs = preparedStatement.executeQuery();

            if (!rs.isBeforeFirst()) {
                System.out.println("No data");
            } else {
                while (rs.next()) {
                    System.out.println(rs.getString("titre"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
