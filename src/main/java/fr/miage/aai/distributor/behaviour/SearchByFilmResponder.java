package fr.miage.aai.distributor.behaviour;

import fr.miage.aai.distributor.agent.DistributorAgent;
import fr.miage.aai.distributor.model.Distributor;
import fr.miage.aai.distributor.model.Film;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.ReturnInfoFilm;
import fr.miage.aai.utils.models.SearchFilmByTitreModel;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import static fr.miage.aai.utils.Printer.print;

public class SearchByFilmResponder extends CyclicBehaviour {

    private final Distributor distributor = ((DistributorAgent) myAgent).getDistributor();
    MessageTemplate modele;
    public SearchByFilmResponder(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINE MODEL
            SearchFilmByTitreModel ppnm = (SearchFilmByTitreModel) Serializer.deserialize(msg.getContent(), SearchFilmByTitreModel.class);
            if (ppnm == null) {

                return;
            }

            var reponse = msg.createReply();
            reponse.setConversationId("FilmByTitre");

            switch (msg.getPerformative()){
                case ACLMessage.AGREE -> {
                    if(distributor.haveFilmInCatalogue(ppnm.getTitre())){
                        Film film = distributor.getCatalogue().get(ppnm.getTitre());
                        double price = film.getPriceSell() * (1 - distributor.getPercentSales());
                        distributor.setTresorerie(distributor.getTresorerie() + price);
                    }
                }
                case ACLMessage.INFORM -> {
                    if(distributor.haveFilmInCatalogue(ppnm.getTitre())){
                        Film film = distributor.getCatalogue().get(ppnm.getTitre());
                        double price = film.getPriceSell() * (1 - distributor.getPercentSales());
                        ReturnInfoFilm infoFilm = new ReturnInfoFilm(film.getName(), film.isAbonnement(), film.isDownload(), price);

                        reponse.setPerformative(ACLMessage.PROPOSE);
                        reponse.setContent(Serializer.serialize(infoFilm));
                        reponse.setInReplyTo(msg.getReplyWith());
                        reponse.setConversationId(msg.getConversationId());
                        reponse.setReplyWith(msg.getReplyWith());
                        print("Film trouvé : " + infoFilm.getTitre(), "distributor", myAgent.getLocalName());
                        myAgent.send(reponse);
                    }
                    reponse.setPerformative(ACLMessage.REFUSE);
                    reponse.setReplyWith(msg.getReplyWith());
                    myAgent.send(reponse);
                }
            }
        }
        else block();
    }

}
