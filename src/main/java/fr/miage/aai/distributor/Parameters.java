package fr.miage.aai.distributor;

public interface Parameters {
    boolean DISTRIBUTOR_IS_SALES = false;
    double DISTIBUTOR_TRESORERIE = 75000;
    double DISTRIBUTOR_PERCENT_SALES = 0.1;
    double DISTRIBUTOR_PRICE_ABO = 20;
    int DISTRIBUTOR_CAPACITY_STORAGE_MAX = 30;
    int DISTRIBUTOR_NB_ABO = 1000;
    int DISTRIBUTOR_NB_DAY_SALES = 20;
}
