package fr.miage.aai.distributor.database;

import fr.miage.aai.distributor.model.Actor;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseConnexion {
    private static DatabaseConnexion instance = null;
    private Connection connection = null;

    private DatabaseConnexion() {
        try (BufferedReader br = new BufferedReader(new java.io.FileReader("src/main/resources/database.properties"))) {
            String line;
            StringBuilder url = new StringBuilder("jdbc:mysql://");
            String user = null;
            String password = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("URL")) {
                    url.append(line.split("=")[1]);
                } else if (line.startsWith("DATABASE")) {
                    url.append("/").append(line.split("=")[1]);
                } else if (line.startsWith("USER")) {
                    user = line.split("=")[1];
                } else if (line.startsWith("PASSWORD")) {
                    password = line.split("=")[1];
                }
            }
            connection = DriverManager.getConnection(url.toString(), user, password);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnexion();
        }
        return instance.getConnection();
    }


    public static ArrayList<Actor> getActors(String filmName) {
        Connection connection = getInstance();
        try {
            PreparedStatement statement = connection.prepareStatement("""
                    select c.* from casting c
                    join movies_actors ma on ma.id_person = c.id
                    join perso_movies pm on pm.id_film = ma.id_movie
                    where pm.titre = ?
                    """);
            statement.setString(1, filmName);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Actor> actors = new ArrayList<>();
            while (resultSet.next()) {
                actors.add(new Actor(resultSet.getString("name")));
            }
            return actors;
        } catch (SQLException e) {
            return null;
        }
    }

    public static ArrayList<String> getGenre(String filmName) {
        Connection connection = getInstance();
        try {
            PreparedStatement statement = connection.prepareStatement("""
                    select mg.genre 
                    from movies_genres mg join perso_movies pm on pm.id_film = mg.id 
                    where pm.titre = ?
                    """);
            statement.setString(1, filmName);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<String> genres = new ArrayList<>();
            if (resultSet.next()) {
                genres.add(resultSet.getString("genre"));
            }
            return genres;
        } catch (SQLException e) {
            return null;
        }
    }

    private Connection getConnection() {
        return this.connection;
    }
}
