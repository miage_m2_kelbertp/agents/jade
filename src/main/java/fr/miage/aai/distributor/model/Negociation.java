package fr.miage.aai.distributor.model;

import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public abstract class Negociation {

    public String film;
    public boolean exclusive;

    public double offre;
    public double offreAutre;
    public double offrePrecedente;
    public double prixSouhaite = 60;
    public double epsilon = 0.1;
    public double seuil = 110;
    public boolean accord;
    public boolean rejet;
    public int step = 0;

    public abstract PriceProposalNegociationModel negociate(PriceProposalNegociationModel ppnm, int step, Distributor distributor);
}
