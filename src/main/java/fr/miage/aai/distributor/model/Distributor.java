package fr.miage.aai.distributor.model;

import fr.miage.aai.distributor.Parameters;
import fr.miage.aai.distributor.database.DatabaseConnexion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class Distributor {
    private final String name;
    private final String id;
    private int nbAbo;
    private double priceAbo;
    private final boolean isOnlyAbonnement;
    private int nbDaySales;
    private boolean isSales;
    private double percentSales;
    private double tresorerie;
    private int capacityStorageMax;
    private HashMap<String, Film> catalogue = new HashMap<>();

    private HashMap<String, Film> backlist = new HashMap<>();

    public Distributor(String name) {
        this.name = name;
        this.id = UUID.randomUUID().toString();
        this.nbAbo = Parameters.DISTRIBUTOR_NB_ABO;
        this.priceAbo = Parameters.DISTRIBUTOR_PRICE_ABO;
        this.tresorerie = Parameters.DISTIBUTOR_TRESORERIE;
        this.capacityStorageMax = Parameters.DISTRIBUTOR_CAPACITY_STORAGE_MAX;
        this.nbDaySales = Parameters.DISTRIBUTOR_NB_DAY_SALES;
        this.isSales = Parameters.DISTRIBUTOR_IS_SALES;
        this.percentSales = Parameters.DISTRIBUTOR_PERCENT_SALES;

        Random random = new Random();
        this.isOnlyAbonnement = random.nextInt(100) < 30;
    }

    public String getName() {
        return name;
    }

    public double getTresorerie() {
        return tresorerie;
    }

    public void addFilmToCatalog(Film film) {

        film.setActors(DatabaseConnexion.getActors(film.getName()));
        film.setGenre(DatabaseConnexion.getGenre(film.getName()));

        film.setAbonnement(true);
        if (!isOnlyAbonnement) {
            Random random = new Random();
            if (random.nextInt(100) < 50 && getPercentOfDownload() < 20.0) {
                film.setDownload(true);
                film.setPriceSell(Math.min(film.getPriceBuy() * 0.001, 8.0));
                catalogue.put(film.getName(), film);
                return;
            }
            film.setDownload(false);
        }
        catalogue.put(film.getName(), film);
    }

    public boolean isFull() {
        return capacityStorageMax <= catalogue.size();
    }

    public int getNbAbo() {
        return nbAbo;
    }

    public Distributor setNbAbo(int nbAbo) {
        this.nbAbo = nbAbo;
        return this;
    }

    public double getPriceAbo() {
        return priceAbo;
    }

    public boolean isSales() {
        return isSales;
    }

    public double getPercentSales() {
        return isSales ? percentSales : 0;
    }

    public void setPercentSales(double percentSales) {
        this.percentSales = percentSales;
    }

    public boolean haveFilm(String film) {
        return catalogue.containsKey(film);
    }

    public void addFilmToBackList(Film film) {
        this.backlist.put(film.getName(), film);
    }

    public int getNbDaySales() {
        return nbDaySales;
    }

    public Distributor setNbDaySales(int nbDaySales) {
        this.nbDaySales = nbDaySales;
        return this;
    }

    public Distributor setSales(boolean sales) {
        isSales = sales;
        return this;
    }

    public Distributor setTresorerie(double tresorerie) {
        this.tresorerie = tresorerie;
        return this;
    }

    public Distributor setCapacityStorageMax(int capacityStorageMax) {
        this.capacityStorageMax = capacityStorageMax;
        return this;
    }

    public Distributor setCatalogue(HashMap<String, Film> catalogue) {
        this.catalogue = catalogue;
        return this;
    }

    public Distributor setBacklist(HashMap<String, Film> backlist) {
        this.backlist = backlist;
        return this;
    }


    public boolean haveFilmInBacklist(String film) {
        return backlist.containsKey(film);
    }

    public HashMap<String, Film> getBacklist() {
        return backlist;
    }

    public boolean haveFilmInCatalogue(String film) {
        return catalogue.containsKey(film);
    }

    public HashMap<String, Film> getCatalogue() {
        return catalogue;
    }

    public ArrayList<String> searchFilmsByActorAndGenre(String genre, String actor) {
        ArrayList<String> films = new ArrayList<>();
        for (Film film : catalogue.values()) {
            if (film.getGenre().stream().anyMatch(g -> g.equals(genre)) || film.getActors().stream().anyMatch(mv -> mv.getName().equals(actor))) {
                films.add(film.getName());
            }
        }
        return films;
    }

    public double getPercentOfDownload() {
        int nbFilmDownload = 0;
        int nbFilmTotal = 0;
        for (Film film : catalogue.values()) {
            nbFilmTotal++;
            if (film.isDownload()) {
                nbFilmDownload++;
            }
        }
        return (double) nbFilmDownload / nbFilmTotal;
    }

    public void setPriceAbo(double priceAbo) {
        this.priceAbo = priceAbo;
    }
}
