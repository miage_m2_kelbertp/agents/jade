package fr.miage.aai.distributor.model;

public class Actor {
    private final String name;

    public Actor(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }
}
