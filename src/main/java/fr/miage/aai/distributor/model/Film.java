package fr.miage.aai.distributor.model;

import java.util.ArrayList;
import java.util.UUID;

public class Film {
    private final String id;
    private String name;
    private String producer;

    private ArrayList<String> Genre;
    private ArrayList<Actor> actors;
    private double priceBuy;
    private int pricePropose;
    private double priceSell;
    private double famous;
    private boolean isExclusivity;
    private boolean isAbonnement;
    private boolean isDownload;

    public Film() {
        this.id = UUID.randomUUID().toString();
    }

    public Film(String name, double price, boolean exclu, String producer) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.pricePropose = (int) price;
        this.isExclusivity = exclu;
        this.producer = producer;
    }

    public Film(String name, String producer, double price) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.producer = producer;
        this.priceBuy = price;
    }

    public Film(String film, String sender) {
        this.id = UUID.randomUUID().toString();
        this.name = film;
        this.producer = sender;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Film setName(String name) {
        this.name = name;
        return this;
    }

    public String getProducer() {
        return producer;
    }

    public Film setProducer(String producer) {
        this.producer = producer;
        return this;
    }

    public double getPriceBuy() {
        return priceBuy;
    }

    public Film setPriceBuy(double priceBuy) {
        this.priceBuy = priceBuy;
        return this;
    }

    public int getPricePropose() {
        return pricePropose;
    }

    public Film setPricePropose(int pricePropose) {
        this.pricePropose = pricePropose;
        return this;
    }

    public double getFamous() {
        return famous;
    }

    public Film setFamous(double famous) {
        this.famous = famous;
        return this;
    }

    public boolean isExclusivity() {
        return isExclusivity;
    }

    public Film setExclusivity(boolean exclusivity) {
        isExclusivity = exclusivity;
        return this;
    }

    public boolean isAbonnement() {
        return isAbonnement;
    }

    public Film setAbonnement(boolean abonnement) {
        isAbonnement = abonnement;
        return this;
    }

    public boolean isDownload() {
        return isDownload;
    }

    public Film setDownload(boolean download) {
        isDownload = download;
        return this;
    }

    public double getPriceSell() {
        return priceSell;
    }

    public Film setPriceSell(double priceSell) {
        this.priceSell = priceSell;
        return this;
    }

    public ArrayList<String> getGenre() {
        return Genre;
    }

    public ArrayList<Actor> getActors() {
        return actors;
    }

    @Override
    public String toString() {
        return " | Producteur : " + producer;
    }

    public void setGenre(ArrayList<String> genre) {
        Genre = genre;
    }

    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }
}
