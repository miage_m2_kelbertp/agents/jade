package fr.miage.aai.utils;

public class Printer {
    private static final String DISTRIBUTOR_COLOR = "\u001B[35m";
    private static final String PRODUCTOR_COLOR = "\u001B[31m";
    private static final String REPUTATION_COLOR = "\u001B[33m";
    private static final String RATING_COLOR = "\u001B[39m";

    public static void print(String message, String agentType) {
        String color = getColor(agentType);
        String emoji = getEmoji(agentType);
        System.out.println(color + emoji + message + "\u001B[0m");
    }

    private static String getEmoji(String agentType) {
        return switch (agentType) {
            case "distributor" -> "📺 ";
            case "productor" -> "🎬 ";
            case "reputation" -> "📈 ";
            default -> "";
        };
    }

    public static void print(String message, String agentType, String agentName) {
        String color = getColor(agentType);
        String emoji = getEmoji(agentType);
        System.out.println(color + emoji + agentName + " : " + message + "\u001B[0m");
    }

    private static String getColor(String agentType) {
        return switch (agentType) {
            case "distributor" -> DISTRIBUTOR_COLOR;
            case "productor" -> PRODUCTOR_COLOR;
            case "reputation" -> REPUTATION_COLOR;
            case "rating" -> RATING_COLOR;
            default -> "";
        };
    }
}
