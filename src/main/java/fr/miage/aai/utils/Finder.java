package fr.miage.aai.utils;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.ArrayList;
import java.util.List;

public class Finder {
    public static List<AID> find(Agent myAgent, String service) {
        ArrayList<AID> agents = new ArrayList<>();

        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(service);
        dfd.addServices(sd);
        DFAgentDescription[] result;

        try {
            result = DFService.search(myAgent, dfd);
            for (DFAgentDescription agentdesc : result) {
                agents.add(agentdesc.getName());
            }
        } catch (FIPAException e) {
            throw new RuntimeException(e);
        }

        return agents;
    }
}
