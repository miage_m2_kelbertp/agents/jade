package fr.miage.aai.utils.models;

public class FetchActorRating {
    private Integer actorId;

    public FetchActorRating(Integer actorId) {
        this.actorId = actorId;
    }

    public Integer getActorId() {
        return actorId;
    }

    public void setActorId(Integer actorId) {
        this.actorId = actorId;
    }
}
