package fr.miage.aai.utils.models;

public class SearchFilmByTitreModel {
    private String titre;

    public SearchFilmByTitreModel(String titre) {
        this.titre = titre;
    }

    public String getTitre() {
        return titre;
    }
}
