package fr.miage.aai.utils.models;

public class FetchMovieRating {
    private Integer movieId;

    public FetchMovieRating(Integer movieId) {
        this.movieId = movieId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }
}
