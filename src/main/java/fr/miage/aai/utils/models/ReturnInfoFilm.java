package fr.miage.aai.utils.models;

public class ReturnInfoFilm {

    private String titre;
    private boolean isAbonnement;
    private boolean isDownload;
    private double priceSell;

    public ReturnInfoFilm(String titre, boolean isAbonnement, boolean isDownload, double priceSell) {
        this.titre = titre;
        this.isAbonnement = isAbonnement;
        this.isDownload = isDownload;
        this.priceSell = priceSell;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean isAbonnement() {
        return isAbonnement;
    }

    public void setAbonnement(boolean abonnement) {
        isAbonnement = abonnement;
    }

    public boolean isDownload() {
        return isDownload;
    }

    public void setDownload(boolean download) {
        isDownload = download;
    }

    public double getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(double priceSell) {
        this.priceSell = priceSell;
    }
}
