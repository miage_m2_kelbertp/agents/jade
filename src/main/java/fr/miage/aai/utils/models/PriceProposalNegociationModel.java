package fr.miage.aai.utils.models;

public class PriceProposalNegociationModel {

    private String film;

    private double price;

    private boolean exclu;

    public PriceProposalNegociationModel(String film, double price, boolean exclu) {
        this.film = film;
        this.price = price;
        this.exclu = exclu;
    }

    public String getFilm() {
        return film;
    }

    public PriceProposalNegociationModel setFilm(String film) {
        this.film = film;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public PriceProposalNegociationModel setPrice(double price) {
        this.price = price;
        return this;
    }

    public boolean getExclu() {
        return exclu;
    }

    public PriceProposalNegociationModel setExclu(boolean exclu) {
        this.exclu = exclu;
        return this;
    }
}
