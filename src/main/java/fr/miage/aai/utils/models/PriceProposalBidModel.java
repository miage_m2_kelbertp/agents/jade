package fr.miage.aai.utils.models;

/**
 * Distributor's model to productors
 */
public class PriceProposalBidModel {

    private int proposal;

    public PriceProposalBidModel(int proposal) {
        this.proposal = proposal;
    }
}
