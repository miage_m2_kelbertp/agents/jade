package fr.miage.aai.utils.models;


import java.util.ArrayList;

public class ReturnFilms {
    private ArrayList<String> films;

    public ReturnFilms(ArrayList<String> films) {
        this.films = films;
    }

    public ArrayList<String> getFilms() {
        return films;
    }

    public void setFilms(ArrayList<String> films) {
        this.films = films;
    }
}
