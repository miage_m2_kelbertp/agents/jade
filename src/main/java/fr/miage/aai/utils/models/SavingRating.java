package fr.miage.aai.utils.models;

public class SavingRating {
    private int movieId;
    private String client;
    private Double rating;

    public SavingRating(int movieId, String client, Double rating) {
        this.movieId = movieId;
        this.client = client;
        this.rating = rating;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
