package fr.miage.aai.utils.models;

public class ReturnMovieRating {
    private Integer movieId;
    private Double rating;
    private Integer nbRatings;
    private Double desirability;
    private String name;

    private boolean foundAtLeastOneRating = false;

    public ReturnMovieRating(Integer movieId, Double rating, Integer nbRatings, boolean foundAtLeastOneRating) {
        this.movieId = movieId;
        this.rating = rating;
        this.nbRatings = nbRatings;
        this.foundAtLeastOneRating = foundAtLeastOneRating;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getNbRatings() {
        return nbRatings;
    }

    public void setNbRatings(Integer nbRatings) {
        this.nbRatings = nbRatings;
    }

    public Double getDesirability() {
        return desirability;
    }

    public void setDesirability(Double desirability) {
        this.desirability = desirability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFoundAtLeastOneRating() {
        return foundAtLeastOneRating;
    }

    public void setFoundAtLeastOneRating(boolean foundAtLeastOneRating) {
        this.foundAtLeastOneRating = foundAtLeastOneRating;
    }
}
