package fr.miage.aai.utils.models;

public class SearchFilmByAGModel {
    private String actor;
    private String genre;

    public SearchFilmByAGModel(String actor, String genre) {
        this.actor = actor;
        this.genre = genre;
    }

    public String getActor() {
        return actor;
    }

    public String getGenre() {
        return genre;
    }

}
