package fr.miage.aai.utils.models;

public class SubscriptionTransaction {

    public enum Type {
        askPrice, proposePrice, unsubscribe, subscribe
    }
    private String nomInternaute;
    private Type type;
    private Double price = 0.00;

    public SubscriptionTransaction(Type t, String nomClient) {
        this.type = t;
        this.nomInternaute = nomClient;
    }

    public SubscriptionTransaction(String nomInternaute, Type type, Double price) {
        this.nomInternaute = nomInternaute;
        this.type = type;
        this.price = price;
    }

    public SubscriptionTransaction(Type type, Double price) {
        this.type = type;
        this.price = price;
    }

    public String getNomInternaute() {
        return nomInternaute;
    }

    public void setNomInternaute(String nomInternaute) {
        this.nomInternaute = nomInternaute;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
