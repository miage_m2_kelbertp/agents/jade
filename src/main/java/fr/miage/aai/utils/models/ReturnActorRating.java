package fr.miage.aai.utils.models;

public class ReturnActorRating {
    private Integer idActor;
    private Double rating;
    private boolean foundAtLeastOneRating;

    public ReturnActorRating(Integer idActor, Double rating, boolean foundAtLeastOneRating) {
        this.idActor = idActor;
        this.rating = rating;
        this.foundAtLeastOneRating = foundAtLeastOneRating;
    }

    public Integer getIdActor() {
        return idActor;
    }

    public void setIdActor(Integer idActor) {
        this.idActor = idActor;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public boolean isFoundAtLeastOneRating() {
        return foundAtLeastOneRating;
    }

    public void setFoundAtLeastOneRating(boolean foundAtLeastOneRating) {
        this.foundAtLeastOneRating = foundAtLeastOneRating;
    }
}
