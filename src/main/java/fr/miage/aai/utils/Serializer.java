package fr.miage.aai.utils;

import com.google.gson.Gson;

public class Serializer {
    public static String serialize(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public static Object deserialize(String string, Class<?> classType) {
        Gson gson = new Gson();
        return gson.fromJson(string, classType);
    }
}
