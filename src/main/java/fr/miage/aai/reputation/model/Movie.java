package fr.miage.aai.reputation.model;

public class Movie {
    private int idFilm;
    private String titre;
    private int year;
    private int idMES;
    private String genre;
    private String resume;
    private String codePays;

    public Movie(int idFilm, String titre, int year, String genre, String resume) {
        this.idFilm = idFilm;
        this.titre = titre;
        this.year = year;
        this.idMES = idMES;
        this.genre = genre;
        this.resume = resume;
        this.codePays = codePays;
    }

    public String getIdFilm() {
        return String.valueOf(idFilm);
    }

    public void setIdFilm(int idFilm) {
        this.idFilm = idFilm;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIdMES() {
        return idMES;
    }

    public void setIdMES(int idMES) {
        this.idMES = idMES;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }
}
