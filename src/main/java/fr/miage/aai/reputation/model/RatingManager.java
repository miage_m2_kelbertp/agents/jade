package fr.miage.aai.reputation.model;

import fr.miage.aai.utils.models.ReturnMovieRating;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class RatingManager {
    private HashMap<String, Vector<Rating>> hm = new HashMap<>();

    public void addRating(Rating r) {
        if(hm.containsKey(r.getMovieId())) {
            hm.get(r.getMovieId()).add(r);
        } else {
            Vector<Rating> vector = new Vector<>();
            vector.add(r);
            hm.put(r.getMovieId(), vector);
        }
    }

    public ReturnMovieRating getRatingMovie(Integer idFilm) {
        boolean foundAtLeastOneRating = false;
        int ratingForThisMovieCount = 0;
        double rating = 0;
        String stringId = String.valueOf(idFilm);

        if (hm.containsKey(stringId)) {
            Vector<Rating> v = hm.get(stringId);
            ratingForThisMovieCount = v.size();

            for(Rating r : v) {
                rating = (rating + r.getRating());
            }

            rating = rating / Double.valueOf(ratingForThisMovieCount);
            foundAtLeastOneRating = true;
        }


        return new ReturnMovieRating(Integer.valueOf(idFilm), rating, ratingForThisMovieCount, foundAtLeastOneRating);
    }

    public HashMap<String, Vector<Rating>> getHm() {
        return hm;
    }

    public void setHm(HashMap<String, Vector<Rating>> hm) {
        this.hm = hm;
    }

    public Integer getRatingTotalNumber() {
        int count = 0;
        for (Vector<Rating> vRating : hm.values()) {
            count = count + vRating.size();
        }

        return count;
    }
}
