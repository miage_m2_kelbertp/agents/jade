package fr.miage.aai.reputation.model;

import java.util.*;
import java.util.stream.Collectors;

public class MovieManager {
    private ArrayList<Movie> movies = new ArrayList<>();

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public Map<String, String> getMovieRating(String idFilm) {
        Map<String,String> map = new HashMap<String,String>();

        Movie movie = movies.stream().filter(mv -> mv.getIdFilm().equals(idFilm)).findFirst().orElse(null);

        return map;
    }

    public List<Movie> getMoviesByGenre(String genre) {
        return movies.stream().filter(mv -> mv.getGenre().equals(genre)).collect(Collectors.toList());
    }
}
