package fr.miage.aai.reputation.model;

import java.util.ArrayList;
import java.util.Vector;

public class ClientProfileManager {
    private ArrayList<ClientProfile> v = new ArrayList<>();

    public void add(ClientProfile cp) {
        v.add(cp);
    }

    public void remove(ClientProfile cp) {
        v.remove(cp);
    }

    public ClientProfile getClientProfileByName(String name) {
        for(ClientProfile cp : v) {
            if(cp.getName().equals(name))
                return cp;
        }

        return null;
    }

    public ClientProfile getRandomClientProfile() {
        if(v.isEmpty())
            return null;

        int number = (int) (Math.random() * v.size());
        return v.get(number);
    }

    public ArrayList<ClientProfile> getV() {
        return v;
    }

    public void setV(ArrayList<ClientProfile> v) {
        this.v = v;
    }
}
