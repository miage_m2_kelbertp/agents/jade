package fr.miage.aai.reputation.model;

import jade.core.AID;

public class Subscription {
    private AID distributor;
    private double prix;

    public Subscription(AID distributor, double prix) {
        this.distributor = distributor;
        this.prix = prix;
    }

    public AID getDistributor() {
        return distributor;
    }

    public void setDistributor(AID distributor) {
        this.distributor = distributor;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
}
