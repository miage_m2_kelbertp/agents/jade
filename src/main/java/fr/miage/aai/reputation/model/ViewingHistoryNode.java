package fr.miage.aai.reputation.model;

import java.util.Date;

public class ViewingHistoryNode {
    private Date viewingDate;
    private Rating rating;
    private String movieId;

    public ViewingHistoryNode(Date viewingDate, Rating rating, String movieId) {
        this.viewingDate = viewingDate;
        this.rating = rating;
        this.movieId = movieId;
    }

    public Date getViewingDate() {
        return viewingDate;
    }

    public void setViewingDate(Date viewingDate) {
        this.viewingDate = viewingDate;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }
}
