package fr.miage.aai.reputation.model;

import fr.miage.aai.utils.models.SubscriptionTransaction;
import jade.core.AID;

import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class SubscriptionManager {

    private ArrayList<Subscription> subscriptions = new ArrayList<>();

    public void add(Subscription distributeur) {
        subscriptions.add(distributeur);
    }

    public void remove(Subscription distributeur) {
        subscriptions.remove(distributeur);
    }

    public boolean isSubscribedTo(AID distributor) {
        for(Subscription sub : subscriptions) {
            if (sub.getDistributor().equals(distributor))
                return true;
        }
        return false;
    }

    public Subscription getSubscription(AID distributor) {
        for(Subscription sub : subscriptions) {
            if (sub.getDistributor().equals(distributor))
                return sub;        }

        return null;
    }

    public AID getRandomSubscriptionAID() {
        return subscriptions.get(ThreadLocalRandom.current().nextInt(subscriptions.size())).getDistributor();
    }

    public ArrayList<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public ArrayList<AID> getSubscriptionsAID() {
        ArrayList<AID> aids = new ArrayList<>();

        for (Subscription one : subscriptions) {
            aids.add(one.getDistributor());
        }

        return aids;
    }

    public void removeAID(AID distri) {
        subscriptions.remove(getSubscription(distri));
    }
}
