package fr.miage.aai.reputation.model;

public class Artiste {
    private Integer idArtiste;
    private String nom;

    public Artiste(Integer idArtiste, String nom) {
        this.idArtiste = idArtiste;
        this.nom = nom;
    }

    public Integer getIdArtiste() {
        return idArtiste;
    }

    public void setIdArtiste(Integer idArtiste) {
        this.idArtiste = idArtiste;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
