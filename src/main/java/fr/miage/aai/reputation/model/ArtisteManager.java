package fr.miage.aai.reputation.model;

import java.util.Vector;

public class ArtisteManager {
    private Vector<Artiste> artistes = new Vector<>();

    public ArtisteManager() {
        this.artistes = artistes;
    }

    public Vector<Artiste> getArtistes() {
        return artistes;
    }

    public void setArtistes(Vector<Artiste> artistes) {
        this.artistes = artistes;
    }

    public Artiste getRandomArtiste() {
        int number = (int) (Math.random() * artistes.size());

        return artistes.get(number);
    }
}
