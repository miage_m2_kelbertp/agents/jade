package fr.miage.aai.reputation.model;

import jade.core.AID;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ClientProfile {
    public static final ArrayList<String> existingMoviesGenre = new ArrayList<>();
    private String name = "Jean";
    private ArrayList<ViewingHistoryNode> viewingHistory = new ArrayList<>();
    private SubscriptionManager subscriptions = new SubscriptionManager();
    private double spender = Math.random();
        private LinkedHashSet<String> preferredGenre = new LinkedHashSet    ();
    private final double budgetByMonth = new Random().nextInt(80-10) + 10;
    private double currentBudget = budgetByMonth;

    public ClientProfile(String name) {
        this.name = name;
        this.preferredGenre.add(ClientProfile.existingMoviesGenre.get(ThreadLocalRandom.current().nextInt(ClientProfile.existingMoviesGenre.size())));
        this.preferredGenre.add(ClientProfile.existingMoviesGenre.get(ThreadLocalRandom.current().nextInt(ClientProfile.existingMoviesGenre.size())));
        this.preferredGenre.add(ClientProfile.existingMoviesGenre.get(ThreadLocalRandom.current().nextInt(ClientProfile.existingMoviesGenre.size())));
    }

    public void addSubscription(Subscription distributeur) {
        subscriptions.add(distributeur);
    }

    public void removeSubscription(Subscription distributeur) {
        subscriptions.remove(distributeur);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSpender() {
        return spender;
    }

    public void setSpender(double spender) {
        this.spender = spender;
    }

    public LinkedHashSet<String> getPreferredGenre() {
        return preferredGenre;
    }

    public void setPreferredGenre(LinkedHashSet<String> preferredGenre) {
        this.preferredGenre = preferredGenre;
    }

    public ArrayList<ViewingHistoryNode> getViewingHistory() {
        return viewingHistory;
    }

    public void setViewingHistory(ArrayList<ViewingHistoryNode> viewingHistory) {
        this.viewingHistory = viewingHistory;
    }

    public SubscriptionManager getSubscriptionsManager() {
        return subscriptions;
    }

    public void setSubscriptions(SubscriptionManager subscriptions) {
        this.subscriptions = subscriptions;
    }

    public double getBudgetByMonth() {
        return budgetByMonth;
    }

    public double getCurrentBudget() {
        return currentBudget;
    }

    public void setCurrentBudget(double currentBudget) {
        this.currentBudget = currentBudget;
    }

    public void removeSubscription(AID distri) {
        subscriptions.removeAID(distri);
    }
}
