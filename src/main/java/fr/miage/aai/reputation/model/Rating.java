package fr.miage.aai.reputation.model;

public class Rating {
    private Double rating = 4.00;
    private String author = null;
    private String movieId = "1";

    public Rating(Double rating, String author, String movieId) {
        this.rating = rating;
        this.author = author;
        this.movieId = movieId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }
}
