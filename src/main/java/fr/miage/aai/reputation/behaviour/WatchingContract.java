package fr.miage.aai.reputation.behaviour;

import fr.miage.aai.reputation.agent.ReputationAgent;
import fr.miage.aai.reputation.model.*;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.*;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class WatchingContract extends ContractNetInitiator {
    private ReputationAgent mySpecificAgent;
    private ClientProfile currentClient;
    private MovieManager movieManager;
    private Vector<AID> distributorsKnown;
    private ArtisteManager artistesManager;

    private String typeRequest = "";

    public WatchingContract(ReputationAgent agent, ACLMessage msg, ClientProfile currentClient, MovieManager movies, Vector<AID> distributorsKnown, ArtisteManager artistesManager) {
        super(agent, msg);
        this.mySpecificAgent = agent;
        this.currentClient = currentClient;
        this.movieManager = movies;
        this.distributorsKnown = distributorsKnown;
        this.artistesManager = artistesManager;
    }

    protected Vector prepareCfps(ACLMessage cfp) {
        cfp.clearAllReceiver();
        cfp.setPerformative(ACLMessage.INFORM);

        double proba = (Math.random());

        if (proba <= 0.50) {
            List<Movie> mvList = new ArrayList<>();
            this.typeRequest = "titre";

            for (String genre : currentClient.getPreferredGenre()) {
                mvList.addAll(movieManager.getMoviesByGenre(genre));
            }

            if(mvList.size() > 0) {
                Movie movieSelected = mvList.get(ThreadLocalRandom.current().nextInt(mvList.size()));

                Printer.print(currentClient.getName() + " a envie de regarder " + movieSelected.getTitre(), "reputation");

                cfp.setContent(Serializer.serialize(new SearchFilmByTitreModel(movieSelected.getTitre())));
                cfp.setConversationId("FilmByTitre");
            }
        } else {
            double probaDeux = (Math.random());
            typeRequest = "ag";
            SearchFilmByAGModel sfbm;
            if (proba <= 0.50) {
                String randomGenre = currentClient.getPreferredGenre().stream().skip(ThreadLocalRandom.current().nextInt(currentClient.getPreferredGenre().size())).findFirst().orElse("Action");
                sfbm = new SearchFilmByAGModel("", randomGenre);
                Printer.print("Je cherche des films avec le genre... " + randomGenre, "reputation");
            } else {
                String acteurSelected = artistesManager.getArtistes().get(ThreadLocalRandom.current().nextInt(artistesManager.getArtistes().size())).getNom();
                sfbm = new SearchFilmByAGModel(acteurSelected, "");
                Printer.print("Je cherche des films avec l'artiste... " + acteurSelected, "reputation");
            }

            cfp.setContent(Serializer.serialize(sfbm));
            cfp.setConversationId("FilmByAG");
        }

        for (AID distrib : distributorsKnown) {
            cfp.addReceiver(distrib);
        }

        Vector v = new Vector();
        v.add(cfp);
        return v;
    }

    protected void handleAllResponses(Vector responses, Vector acceptances) {
        Printer.print("Les distributeurs sont " + responses.size() + " à proposer quelque chose...", "reputation", myAgent.getLocalName());

        if (typeRequest.equals("titre")) {
            // Evaluate proposals.
            double bestProposal = 999;
            AID bestProposer = null;
            ACLMessage accept = null;
            Enumeration e = responses.elements();
            while (e.hasMoreElements()) {
                ACLMessage msg = (ACLMessage) e.nextElement();
                ReturnInfoFilm proposal = (ReturnInfoFilm) Serializer.deserialize(msg.getContent(), ReturnInfoFilm.class);
                if (msg.getPerformative() == ACLMessage.PROPOSE) {

                    ACLMessage reply = msg.createReply();
                    reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                    acceptances.addElement(reply);


                    if (proposal.getPriceSell() < bestProposal) {
                        bestProposal = proposal.getPriceSell();
                        bestProposer = msg.getSender();
                        accept = reply;
                    } else {
                        Printer.print("Le distributeur " + msg.getSender() + " ne peut pas satisfaire la demande du client.", "reputation");
                    }
                }
            }
            // Accept the proposal of the best proposer
            if (accept != null) {
                Printer.print("Acceptation de la proposition de " + bestProposer.getName() + " pour " + bestProposal + "€", "productor", myAgent.getLocalName());
                accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);

                //accept.setPerformative(ACLMessage.PROPOSE);
                accept.setContent(bestProposal - 1 + "");
            }
        } else {
            double bestProposal = 999;
            AID bestProposer = null;
            ACLMessage accept = null;
            Enumeration e = responses.elements();
            while (e.hasMoreElements()) {
                ACLMessage msg = (ACLMessage) e.nextElement();
                ReturnFilms proposal = (ReturnFilms) Serializer.deserialize(msg.getContent(), ReturnFilms.class);
                if (msg.getPerformative() == ACLMessage.PROPOSE) {
                    Printer.print("Le distributeur propose les films... : " + String.join(" ", proposal.getFilms()), "reputation");

                }
            }
            // Accept the proposal of the best proposer
            if (accept != null) {
                Printer.print("Acceptation de la proposition de " + bestProposer.getName() + " pour " + bestProposal + "€", "productor", myAgent.getLocalName());
                accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);

                //accept.setPerformative(ACLMessage.PROPOSE);
                accept.setContent(bestProposal - 1 + "");
            }
        }
    }
}