package fr.miage.aai.reputation.behaviour;

import fr.miage.aai.reputation.agent.ReputationAgent;
import fr.miage.aai.reputation.model.ClientProfile;
import fr.miage.aai.reputation.model.Subscription;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.SubscriptionTransaction;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;

import java.util.Vector;

public class SubscriptionContract extends ContractNetInitiator {
    private AID chosenDistributor;
    private ReputationAgent mySpecificAgent;
    private ClientProfile currentClient;

    public SubscriptionContract(ReputationAgent a, ACLMessage cfp, ClientProfile profile, AID distributor) {
        super(a, cfp);
        this.chosenDistributor = distributor;
        this.mySpecificAgent = a;
        this.currentClient = profile;
    }

    protected Vector prepareCfps(ACLMessage cfp) {
        cfp.clearAllReceiver();
        Printer.print(currentClient.getName() + " a soumis une demande de souscription à " + chosenDistributor.getLocalName(), "reputation");
        cfp.addReceiver(chosenDistributor);
        SubscriptionTransaction st = new SubscriptionTransaction(SubscriptionTransaction.Type.askPrice, currentClient.getName());
        cfp.setContent(Serializer.serialize(st));
        cfp.setConversationId("Subscribe");
        Vector v = new Vector();
        v.add(cfp);
        return v;
    }

    protected void handleOutOfSequence(ACLMessage msg) {
        if (msg.getPerformative() == ACLMessage.PROPOSE) {
            SubscriptionTransaction st = (SubscriptionTransaction) Serializer.deserialize(msg.getContent(), SubscriptionTransaction.class);
            try {
                ACLMessage rsp = msg.createReply();
                rsp.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                rsp.setContent(Serializer.serialize(new SubscriptionTransaction(st.getNomInternaute(), SubscriptionTransaction.Type.subscribe, st.getPrice())));
                currentClient.addSubscription(new Subscription(msg.getSender(), st.getPrice()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.getPerformative() == ACLMessage.REFUSE) {
            Printer.print("Le distributeur " + msg.getSender().getLocalName() + " a refusé la souscription de l'abonnement.", "reputation");
        }
    }

    // we accept everything so other groups can work;
    protected void handleAllResponses(Vector responses, Vector acceptances) {
        for (int i = 0; i < responses.size(); i++) {
            ACLMessage answer = (ACLMessage) responses.get(i);
            if (answer.getPerformative() == ACLMessage.PROPOSE) {
                SubscriptionTransaction st = (SubscriptionTransaction) Serializer.deserialize(answer.getContent(), SubscriptionTransaction.class);
                try {
                    ACLMessage rsp = answer.createReply();
                    rsp.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                    rsp.setContent(Serializer.serialize(new SubscriptionTransaction(st.getNomInternaute(), SubscriptionTransaction.Type.subscribe, st.getPrice())));
                    acceptances.add(rsp);
                    currentClient.addSubscription(new Subscription(answer.getSender(), st.getPrice()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (answer.getPerformative() == ACLMessage.REFUSE) {
                Printer.print("Le distributeur " + answer.getSender().getLocalName() + " a refusé la souscription de l'abonnement.", "reputation");
            }
        }

        mySpecificAgent.setSubscriptionInProgress(false);
    }

    protected void handleInform(ACLMessage inform) {
        Printer.print(currentClient.getName() + " s'est bien abonné à " + inform.getSender().getLocalName(), "reputation");
        SubscriptionTransaction st = (SubscriptionTransaction) Serializer.deserialize(inform.getContent(), SubscriptionTransaction.class);
        currentClient.addSubscription(new Subscription(inform.getSender(), st.getPrice()));
    }

    protected void handlePropose(ACLMessage propose, Vector acceptances) {
        SubscriptionTransaction st = (SubscriptionTransaction) Serializer.deserialize(propose.getContent(), SubscriptionTransaction.class);
        Printer.print("Le distributeur "+propose.getSender().getLocalName()+" a proposé un abonnement de " + st.getPrice() + " € à " + currentClient.getName(), "reputation");
    }
}
