package fr.miage.aai.reputation.behaviour;

import fr.miage.aai.reputation.agent.ReputationAgent;
import fr.miage.aai.reputation.model.ClientProfile;
import fr.miage.aai.utils.models.SubscriptionTransaction;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

import java.util.Vector;

import static fr.miage.aai.utils.models.SubscriptionTransaction.Type.unsubscribe;

public class UnsubscriptionContract extends AchieveREInitiator {

    private AID chosenDistributor;
    private ReputationAgent mySpecificAgent;
    private ClientProfile currentClient;

    public UnsubscriptionContract(ReputationAgent a, ACLMessage cfp, ClientProfile profile, AID distributor) {
        super(a, cfp);
        this.chosenDistributor = distributor;
        this.mySpecificAgent = a;
        this.currentClient = profile;
    }

    protected Vector prepareRequests(ACLMessage cfp) {
        cfp.clearAllReceiver();
        Vector messages = new Vector();
        cfp.addReceiver(chosenDistributor);
        cfp.setContent(Serializer.serialize(new SubscriptionTransaction(unsubscribe, currentClient.getName())));
        messages.add(cfp);
        currentClient.removeSubscription(cfp.getSender());
        Printer.print(currentClient.getName() + " a soumis une demande de désouscription à " + chosenDistributor.getLocalName(), "reputation");

        return messages;
    }
    public void handleInform(ACLMessage msg) {
        Printer.print(currentClient.getName() + " s'est bien désabonné à " + msg.getSender().getLocalName(), "reputation");
    }


}

