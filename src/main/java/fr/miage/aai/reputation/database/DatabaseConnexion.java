package fr.miage.aai.reputation.database;

import javax.xml.crypto.Data;
import javax.xml.transform.Result;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;

public class DatabaseConnexion {
    private String url = "";
    private String user = "";
    private String password = "";

    public DatabaseConnexion() {
        try (BufferedReader br = new BufferedReader(new java.io.FileReader("src/main/resources/database.properties"))) {
            String line;
            StringBuilder url = new StringBuilder("jdbc:mysql://");
            String user = null;
            String password = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("URL")) {
                    url.append(line.split("=")[1]);
                } else if (line.startsWith("DATABASE")) {
                    url.append("/").append(line.split("=")[1]);
                } else if (line.startsWith("USER")) {
                    user = line.split("=")[1];
                } else if (line.startsWith("PASSWORD")) {
                    password = line.split("=")[1];
                }
            }

            this.url = url.toString();
            this.user = user;
            this.password = password;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
