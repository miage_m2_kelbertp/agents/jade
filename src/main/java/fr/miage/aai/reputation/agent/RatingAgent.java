package fr.miage.aai.reputation.agent;

import fr.miage.aai.reputation.database.DatabaseConnexion;
import fr.miage.aai.reputation.model.Rating;
import fr.miage.aai.reputation.model.RatingManager;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.*;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.sql.*;
import java.util.Vector;

import static jade.lang.acl.MessageTemplate.and;
import static jade.lang.acl.MessageTemplate.or;

public class RatingAgent extends Agent {
    private final RatingManager rm = new RatingManager();

    protected void setup() {
        addBehaviour(new HandleReviewFetch(this));

        addBehaviour(new TickerBehaviour(this, 10000) {
            protected void onTick() {
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("distributor");
                template.addServices(sd);

                if (rm.getRatingTotalNumber() > 0) {
                    Printer.print("Partage des avis aux distributeurs...", "reputation");

                    try {
                        DFAgentDescription[] result = DFService.search(myAgent, template);
                        for (String movieId : rm.getHm().keySet()) {
                            ACLMessage message = new ACLMessage(ACLMessage.INFORM);
                            message.setConversationId("Rating");
                            for (DFAgentDescription dfAgentDescription : result) {
                                message.addReceiver(dfAgentDescription.getName());
                            }

                            message.setContent(Serializer.serialize(rm.getRatingMovie(Integer.valueOf(movieId))));
                            send(message);
                        }

                    } catch (FIPAException fe) {
                        fe.printStackTrace();
                    }
                }
            }
        });
    }

    private class HandleReviewFetch extends CyclicBehaviour {
        MessageTemplate template = and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
                (or(MessageTemplate.MatchReplyWith("fetch-rating"), MessageTemplate.MatchReplyWith("save-rating"))));

        public HandleReviewFetch(Agent a) {
            super(a);
        }

        @Override
        public void action() {
            final ACLMessage message = myAgent.receive(template);

            if(message != null) {
                if (message.getReplyWith().equals("fetch-rating-movie")) {
                    final ACLMessage answer = new ACLMessage(ACLMessage.INFORM);

                    FetchMovieRating fmr = (FetchMovieRating) Serializer.deserialize(message.getContent(), FetchMovieRating.class);

                    answer.setReplyWith("fetch-rating-movie");
                    answer.setSender(myAgent.getAID());
                    answer.addReceiver(message.getSender());
                    ReturnMovieRating result = rm.getRatingMovie(fmr.getMovieId());
                    answer.setContent(Serializer.serialize(result));
                    send(answer);
                } else if (message.getReplyWith().equals("fetch-rating-actor")) {
                    final ACLMessage answer = new ACLMessage(ACLMessage.INFORM);

                    ResultSet resultSet = null;
                    DatabaseConnexion dc = new DatabaseConnexion();
                    Vector<Integer> movies = new Vector<>();
                    FetchActorRating far = (FetchActorRating) Serializer.deserialize(message.getContent(), FetchActorRating.class);

                    try (Connection connection = DriverManager.getConnection(dc.getUrl(), dc.getUser(), dc.getPassword());
                         PreparedStatement preparedStatement = connection.prepareStatement("SELECT idFilm FROM Role WHERE idActeur = ?")) {
                        preparedStatement.setInt(1, far.getActorId());
                        resultSet = preparedStatement.executeQuery();

                        while (resultSet.next()) {
                            movies.add(resultSet.getInt(1));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    Double rating = 0.00;
                    boolean foundAtLeastOneRating = false;

                    for (Integer idFilm : movies) {
                        ReturnMovieRating movie = rm.getRatingMovie(idFilm);
                        rating = rating + movie.getRating();
                        if (!foundAtLeastOneRating && movie.isFoundAtLeastOneRating())
                            foundAtLeastOneRating = movie.isFoundAtLeastOneRating();
                    }

                    rating = rating / Double.valueOf(movies.size());

                    answer.setReplyWith("fetch-rating-actor");
                    answer.setSender(myAgent.getAID());
                    answer.addReceiver(message.getSender());
                    answer.setContent(Serializer.serialize(new ReturnActorRating(far.getActorId(), rating, foundAtLeastOneRating)));
                    send(answer);

                    for (Integer movieId : movies) {
                        rm.getRatingMovie(movieId);
                    }

                } else if (message.getReplyWith().equals("save-rating")) {
                    final ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
                    SavingRating sr = (SavingRating) Serializer.deserialize(message.getContent(), SavingRating.class);

                    answer.setReplyWith("save-rating");
                    answer.setSender(myAgent.getAID());
                    answer.addReceiver(message.getSender());
                    send(answer);

                    rm.addRating(new Rating(sr.getRating(), sr.getClient(), String.valueOf(sr.getMovieId())));
                }
            }
        }
    }
}
