package fr.miage.aai.reputation.agent;

import com.mysql.cj.xdevapi.Client;
import fr.miage.aai.reputation.behaviour.SubscriptionContract;
import fr.miage.aai.reputation.behaviour.UnsubscriptionContract;
import fr.miage.aai.reputation.behaviour.WatchingContract;
import fr.miage.aai.reputation.database.DatabaseConnexion;
import fr.miage.aai.reputation.model.*;
import fr.miage.aai.utils.Printer;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

public class ReputationAgent extends Agent {
    private Vector<AID> distributorAgentsKnown = new Vector<>();
    private Vector<AID> subscriptions = new Vector<>();
    private boolean subscriptionInProgress = false;
    private ClientProfileManager clients = new ClientProfileManager();
    private MovieManager movies = new MovieManager();

    private ArtisteManager artistes = new ArtisteManager();

    protected void setup() {
        ResultSet resultSet = null;
        DatabaseConnexion dc = new DatabaseConnexion();

        try (Connection connection = DriverManager.getConnection(dc.getUrl(), dc.getUser(), dc.getPassword());
             Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("select distinct(genre)" +
                    " from movies_genres");

            while (resultSet.next()) {
                ClientProfile.existingMoviesGenre.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(dc.getUrl(), dc.getUser(), dc.getPassword());
             Statement statement = connection.createStatement();) {
            resultSet = statement.executeQuery("select pm.id_film, pm.titre, pm.year, mg.genre, pm.resume" +
                    " from perso_movies pm" +
                    " join movies_genres mg on mg.id = pm.id_film");

            while (resultSet.next()) {
                movies.getMovies().add(new Movie(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(dc.getUrl(), dc.getUser(), dc.getPassword());
             Statement statement = connection.createStatement();) {
            resultSet = statement.executeQuery("SELECT * FROM casting");

            while (resultSet.next()) {
                artistes.getArtistes().add(new Artiste(resultSet.getInt(1), resultSet.getString(2)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        clients.add(new ClientProfile("Jean"));
        clients.add(new ClientProfile("Théo"));
        clients.add(new ClientProfile("Paul"));

        addBehaviour(new TickerBehaviour(this, 10000) {
            protected void onTick() {
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("distributor");
                template.addServices(sd);

                try {
                    DFAgentDescription[] result = DFService.search(myAgent, template);
                    distributorAgentsKnown.clear();
                    Printer.print("Envie de regarder un film, allons voir ce que proposent...", "reputation");
                    for (DFAgentDescription dfAgentDescription : result) {
                        distributorAgentsKnown.add(dfAgentDescription.getName());
                        Printer.print(dfAgentDescription.getName().getLocalName(), "reputation");
                    }
                } catch (FIPAException fe) {
                    fe.printStackTrace();
                }
            }
        });

        addBehaviour(new TickerBehaviour(this, 10000) {
            protected void onTick() {
                double proba = (Math.random());

                if (proba <= 0.50) {
                    ClientProfile cl = clients.getRandomClientProfile();
                    ArrayList<AID> outerJoin = new ArrayList<>(distributorAgentsKnown);
                    outerJoin.remove(cl.getSubscriptionsManager().getSubscriptionsAID());
                    AID distributor = outerJoin.get(ThreadLocalRandom.current().nextInt(outerJoin.size()));

                    Printer.print("Début du processsus d'abonnement...", "reputation");
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    myAgent.addBehaviour(new SubscriptionContract(ReputationAgent.this, cfp, cl, distributor));
                }
            }
        });

        addBehaviour(new TickerBehaviour(this, 10000) {
            protected void onTick() {
                double proba = (Math.random());
                ClientProfile cl = null;
                Collections.shuffle(clients.getV());

                for (ClientProfile oneClient : clients.getV()) {
                    if (oneClient.getSubscriptionsManager().getSubscriptions().size() > 0)
                        cl = oneClient;
                }

                if (proba <= 0.50 && cl != null) {
                    AID distributor = cl.getSubscriptionsManager().getRandomSubscriptionAID();
                    Printer.print("Début du processus de désabonnement...", "reputation");
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    myAgent.addBehaviour(new UnsubscriptionContract(ReputationAgent.this, cfp, cl, distributor));
                }
            }
        });

        addBehaviour(new TickerBehaviour(this, 10000) {
            protected void onTick() {
                double proba = (Math.random());
                ClientProfile cl = clients.getRandomClientProfile();
                Collections.shuffle(clients.getV());

                Printer.print("Début du processus de visionnage...", "reputation");
                ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                myAgent.addBehaviour(new WatchingContract(ReputationAgent.this, cfp, cl, movies, distributorAgentsKnown, artistes));
            }
        });
    }

    public Vector<AID> getDistributorAgentsKnown() {
        return distributorAgentsKnown;
    }

    public void setDistributorAgentsKnown(Vector<AID> distributorAgentsKnown) {
        this.distributorAgentsKnown = distributorAgentsKnown;
    }

    public Vector<AID> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Vector<AID> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public boolean isSubscriptionInProgress() {
        return subscriptionInProgress;
    }

    public void setSubscriptionInProgress(boolean subscriptionInProgress) {
        this.subscriptionInProgress = subscriptionInProgress;
    }

    public ClientProfileManager getClientsManager() {
        return clients;
    }
}