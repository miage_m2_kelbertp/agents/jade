// https://github.com/mihaimaruseac/JADE-ARIA/blob/master/src/examples/protocols/ContractNetInitiatorAgent.java

package fr.miage.aai.productor.behaviour;

import fr.miage.aai.productor.agent.ProductorAgent;
import fr.miage.aai.productor.model.Productor;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.ContractNetInitiator;

import java.util.Enumeration;
import java.util.Vector;

// IteratorContractnet

public class FilmProposer extends ContractNetInitiator {

    private final Productor productor = ((ProductorAgent) myAgent).getProductor();
    public FilmProposer(Agent agent, ACLMessage msg) {
        super(agent, msg);
    }

    protected void handlePropose(ACLMessage propose, Vector v) {

        PriceProposalNegociationModel msgRecu = (PriceProposalNegociationModel) Serializer.deserialize(propose.getContent(), PriceProposalNegociationModel.class);
        if(msgRecu == null){
            return;
        }
        Printer.print("L'agent " + propose.getSender().getLocalName() + " a proposé le film " + msgRecu.getFilm() + " pour " + msgRecu.getPrice() + "€. (exclu : " + msgRecu.getExclu() + ")", "productor", myAgent.getLocalName());
    }

    protected void handleRefuse(ACLMessage refuse) {
        Printer.print("L'agent " + refuse.getSender().getLocalName() + " a refusé la proposition.", "productor", myAgent.getLocalName());
    }

    protected void handleFailure(ACLMessage failure) {
        if (failure.getSender().equals(myAgent.getAMS())) {
            // FAILURE notification from the JADE runtime: the receiver
            // does not exist
            Printer.print("L'agent " + failure.getSender().getName() + " n'existe pas.", "productor", myAgent.getLocalName());
        }
        else {
            Printer.print("L'agent " + failure.getSender().getLocalName() + " a échoué.", "productor", myAgent.getLocalName());
        }
        // Immediate failure --> we will not receive a response from this agent
    }

    protected void handleInform(ACLMessage inform) {
        Printer.print("L'agent " + inform.getSender().getLocalName() + " a accepté la proposition.", "productor", myAgent.getLocalName());
    }

    protected void handleAllResponses(Vector responses, Vector acceptances) {
        Printer.print("Réponses reçues : " + responses.size(), "productor", myAgent.getLocalName());
        if (responses.size() < 1) {
            // Some responder didn't reply within the specified timeout
            Printer.print("Temps d'attente trop long: il manque " + (1 - responses.size()) + " responses", "productor", myAgent.getLocalName());
        }

        // Evaluate proposals.
        double bestProposal = -1;
        AID bestProposer = null;
        String film = null;
        ACLMessage accept = null;
        boolean exclu = false;
        Enumeration e = responses.elements();
        while (e.hasMoreElements()) {
            ACLMessage msg = (ACLMessage) e.nextElement();
            if (msg.getPerformative() == ACLMessage.PROPOSE) {
                ACLMessage reply = msg.createReply();
                reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                acceptances.addElement(reply);
                PriceProposalNegociationModel proposal = (PriceProposalNegociationModel) Serializer.deserialize(msg.getContent(), PriceProposalNegociationModel.class);
                if (bestProposal == -1) {
                    bestProposal = productor.getFilms().get(proposal.getFilm()) * 1.2;
                }
                if (proposal.getPrice() > bestProposal) {
                    bestProposal = proposal.getPrice();
                    bestProposer = msg.getSender();
                    film = proposal.getFilm();
                    exclu = proposal.getExclu();
                    accept = reply;
                }
            }
        }
        // Accept the proposal of the best proposer
        if (accept != null) {
            Printer.print("Acceptation de la proposition de " + bestProposer.getLocalName() + " pour " + bestProposal + "€", "productor", myAgent.getLocalName());
            accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);

            //accept.setPerformative(ACLMessage.PROPOSE);
            accept.setContent(Serializer.serialize(new PriceProposalNegociationModel(film, bestProposal, exclu)));

            // SAVE DATA
            if (exclu) {
                productor.addFilmExclusivity(film);
            } else {
                productor.addFilmSold(film);
            }
            productor.addBudget(bestProposal);
        }
    }
}
