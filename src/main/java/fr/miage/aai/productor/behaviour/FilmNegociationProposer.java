package fr.miage.aai.productor.behaviour;

import fr.miage.aai.productor.agent.ProductorAgent;
import fr.miage.aai.productor.model.Productor;
import fr.miage.aai.productor.strategy.IStrategy;
import fr.miage.aai.productor.strategy.Strategy1;
import fr.miage.aai.productor.strategy.Strategy2;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.HashMap;

public class FilmNegociationProposer extends CyclicBehaviour {

    private final Productor productor = ((ProductorAgent) myAgent).getProductor();

    static HashMap<AID, IStrategy> filmN = new HashMap<>();

    MessageTemplate modele;

    public FilmNegociationProposer(Agent a, MessageTemplate model) {
        super(a);
        modele = model;
    }

    @Override
    public void action() {
        var msg = myAgent.receive(modele);
        if (msg != null) {
            // DEFINITION OF MODEL
            PriceProposalNegociationModel msgRecu;
            try {
                msgRecu = (PriceProposalNegociationModel) Serializer.deserialize(msg.getContent(), PriceProposalNegociationModel.class);
                if (msgRecu == null) {
                    filmN.remove(msg.getSender());
                    return;
                }
            } catch (Exception e) {
                filmN.remove(msg.getSender());
                return;
            }

            // WORK WITH MULTI-AGENT AND ALIMENTATION DONNES
            IStrategy negociation = null;
            if (!filmN.containsKey(msg.getSender())) {

                // IF FILM DOESN'T EXIST IN OUR CATALOG
                if (!productor.getFilms().containsKey(msgRecu.getFilm())) {
                    var reponse = msg.createReply();
                    reponse.setPerformative(ACLMessage.FAILURE);
                    filmN.remove(msg.getSender());
                    Printer.print("J'envoie mon refus, je ne connais pas ce film.", "productor", myAgent.getLocalName());
                    myAgent.send(reponse);
                    return;
                }

                // IF FILM EXIST IN OUR CATALOG
                double prixTemp = productor.getFilms().get(msgRecu.getFilm());

                // CHOICE OF RANDOM STRATEGY
                if(prixTemp > 10000){
                    negociation = new Strategy1();
                    negociation.threshold = prixTemp * 1.25;
                }else{
                    negociation = new Strategy2();
                    negociation.threshold = prixTemp * 1.2;
                }
                negociation.priceProd = prixTemp;

                filmN.put(msg.getSender(), negociation);
                negociation.film = msgRecu.getFilm();

                negociation.priceProd = prixTemp;
            }
            negociation = filmN.get(msg.getSender());
            negociation.exclusivity = msgRecu.getExclu();
            negociation.offerReceived = msgRecu.getPrice();

            // DEFINE IF AGREE, REFUSE OR FAILURE
            switch (msg.getPerformative()) {
                case ACLMessage.AGREE -> {
                    negociation.agreement = true;
                    Printer.print("J'envoie mon accord sur le prix de " + negociation.offerReceived, "productor", myAgent.getLocalName());
                }
                case ACLMessage.REFUSE -> negociation.rejection = true;
                case ACLMessage.FAILURE -> negociation.rejection = true;
            }

            // IF NO AGREEMENT AND NO REJECTION
            if (!negociation.agreement && !negociation.rejection) {
                negociation.step++;

                Printer.print("J'ai reçu une offre à " + negociation.offerReceived + " de " + msg.getSender().getLocalName(), "productor", myAgent.getLocalName());

                // STRATEGY OF ACCEPTANCE

                boolean isExclusivity = productor.checkFilmExclusivity(msgRecu.getFilm());
                boolean isSold = productor.checkFilmSold(msgRecu.getFilm());

                PriceProposalNegociationModel msgEnvoye = negociation.negociation(msgRecu, isExclusivity, isSold);

                // IF AGREEMENT
                if (negociation.agreement) {
                    var reponse = msg.createReply();
                    reponse.setContent(String.valueOf(negociation.offerReceived));
                    reponse.setPerformative(ACLMessage.AGREE);
                    filmN.remove(msg.getSender());
                    Printer.print("J'envoie mon accord sur le prix de " + negociation.offerReceived, "productor", myAgent.getLocalName());
                    myAgent.send(reponse);

                    // SAVE DATA
                    if (msgEnvoye.getExclu()) {
                        productor.addFilmExclusivity(msgRecu.getFilm());
                    } else {
                        productor.addFilmSold(msgRecu.getFilm());
                    }
                    productor.addBudget(msgRecu.getPrice());
                }

                // IF REJECTION
                else if (negociation.rejection) {
                    var reponse = msg.createReply();
                    reponse.setPerformative(ACLMessage.REFUSE);
                    filmN.remove(msg.getSender());
                    Printer.print("J'envoie mon refus", "productor", myAgent.getLocalName());
                    myAgent.send(reponse);
                }

                // IF STEP OVER MAXIMUM
                else if (negociation.step >= negociation.maxStep) {
                    var reponse = msg.createReply();
                    reponse.setPerformative(ACLMessage.FAILURE);
                    filmN.remove(msg.getSender());
                    Printer.print("J'envoie mon refus, trop d'offres", "productor", myAgent.getLocalName());
                    myAgent.send(reponse);
                }

                // REPONSE
                else{
                    var reponse = msg.createReply();
                    reponse.setContent(Serializer.serialize(msgEnvoye));
                    myAgent.send(reponse);
                    Printer.print("J'ai envoyé une offre à " + msgEnvoye.getPrice() + " à " + msg.getSender().getLocalName(), "productor", myAgent.getLocalName());
                }
            } else {
                // TODO : Remove film
                filmN.remove(msg.getSender());
            }
        }
        else block();
    }
}
