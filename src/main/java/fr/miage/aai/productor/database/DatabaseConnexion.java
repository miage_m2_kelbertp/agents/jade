package fr.miage.aai.productor.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnexion {
    private static DatabaseConnexion instance = null;
    private Connection connection = null;

    private DatabaseConnexion() {
        try (BufferedReader br = new BufferedReader(new java.io.FileReader("src/main/resources/database.properties"))) {
            String line;
            StringBuilder url = new StringBuilder("jdbc:mysql://");
            String user = null;
            String password = null;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("URL")) {
                    url.append(line.split("=")[1]);
                } else if (line.startsWith("DATABASE")) {
                    url.append("/").append(line.split("=")[1]);
                } else if (line.startsWith("USER")) {
                    user = line.split("=")[1];
                } else if (line.startsWith("PASSWORD")) {
                    password = line.split("=")[1];
                }
            }
            connection = DriverManager.getConnection(url.toString(), user, password);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnexion();
        }
        return instance.getConnection();
    }

    private Connection getConnection() {
        return this.connection;
    }
}
