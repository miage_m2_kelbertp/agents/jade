package fr.miage.aai.productor.database;

import fr.miage.aai.productor.model.Film;
import fr.miage.aai.utils.Printer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmQuery {
    public static final String SELECT_RANDOM_FILM = "SELECT * FROM perso_movies ORDER BY RAND() LIMIT 1";
    public static final String SELECT_RANDOM_FILM_WITH_LIST = "SELECT * FROM perso_movies WHERE titre NOT IN (?) ORDER BY RAND() LIMIT 1";

    public static Film performQuery(String query, String... args) {

        Connection con = DatabaseConnexion.getInstance();

        ResultSet rs;
        try {
            PreparedStatement preparedStatement = con.prepareStatement(query);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setString(i + 1, args[i]);
            }
            rs = preparedStatement.executeQuery();

            if (!rs.isBeforeFirst()) {
                Printer.print("No film found", "productor");
            } else {
                while (rs.next()) {
                    return new Film(
                            rs.getString("titre")
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
