package fr.miage.aai.productor.agent;

import fr.miage.aai.productor.behaviour.FilmNegociationProposer;
import fr.miage.aai.productor.behaviour.FilmProposer;
import fr.miage.aai.productor.database.FilmQuery;
import fr.miage.aai.productor.model.Film;
import fr.miage.aai.productor.model.Productor;
import fr.miage.aai.utils.Finder;
import fr.miage.aai.utils.Printer;
import fr.miage.aai.utils.Serializer;
import fr.miage.aai.utils.models.PriceProposalNegociationModel;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static fr.miage.aai.utils.Register.registerService;

public class ProductorAgent extends Agent {

    private static final String SERVICE = "productor";
    protected Productor productor;
    private LocalDate localDate = LocalDate.now().minusDays(1);

    protected void setup() {

        productor = new Productor(getLocalName(), getLocalName());

        super.addBehaviour(new TickerBehaviour(this, 1000) {
            @Override
            // For each tick...
            protected void onTick() {
                DFAgentDescription dfd = new DFAgentDescription();
                DFAgentDescription[] result;
                try {
                    result = DFService.search(super.myAgent, dfd);
                    for (DFAgentDescription agentdesc : result) {
                        Iterator iter = agentdesc.getAllServices();
                        // For each service...
                        while (iter.hasNext()) {
                            ServiceDescription sd = (ServiceDescription) iter.next();
                            // If the service named "ticker" is found...
                            if (sd.getName().equals("ticker")) {

                                // Ask the ticker agent the current date
                                ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
                                message.addReceiver(agentdesc.getName());
                                message.setContent("What time is it ?");
                                super.myAgent.send(message);

                                // Wait for the answer
                                ACLMessage answer = super.myAgent.blockingReceive();

                                LocalDate ld;
                                try {
                                    ld = (LocalDate) answer.getContentObject();
                                    if (ld == null) return;
                                } catch (Exception e) {
                                    return;
                                }

                                // If the date has changed...
                                if (localDate.getDayOfMonth() != ld.getDayOfMonth()) {
                                    localDate = ld;

                                    // Create a new CFP message
                                    ACLMessage msg = new ACLMessage(ACLMessage.CFP);
                                    // Set the protocol to FIPA-Contract-Net
                                    msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);

                                    if (productor.getBudget() < 5000) {
                                        Printer.print("Je n'ai plus assez d'argent pour acheter de nouveaux films", "productor");
                                        return;
                                    }
                                    // Choose random price between 5000 and 30000
                                    int randomPrice = (int) (Math.random() * 25000 + 5000);

                                    // Get films from database
                                    String inList = productor.getFilms().keySet().stream().map(f -> "'" + f + "'").collect(Collectors.joining(","));

                                    Film film;
                                    if (!inList.isEmpty())
                                        film = FilmQuery.performQuery(FilmQuery.SELECT_RANDOM_FILM_WITH_LIST, inList);
                                    else
                                        film = FilmQuery.performQuery(FilmQuery.SELECT_RANDOM_FILM);

                                    if (Productor.filmsAlreadyCreated.contains(film.getTitle())) {
                                        Printer.print("Film déjà créé, impossible de le recréer.", "productor");
                                        return;
                                    }

                                    // Choose a random true/false value
                                    boolean random = Math.random() < 0.5;

                                    // Add new film to productor's films
                                    productor.addFilm(film.getTitle(), randomPrice);
                                    // Decrease budget
                                    productor.addBudget(-randomPrice);

                                    // Increase randomPrice to get a better price
                                    randomPrice += (int) (Math.random() * 10000 + 1000);

                                    Printer.print("Je mets aux enchères le film " + film.getTitle() + " pour " + randomPrice + "€", "productor", myAgent.getLocalName());

                                    // Send the CFP message to all distributors
                                    msg.setContent(Serializer.serialize(new PriceProposalNegociationModel(film.getTitle(), randomPrice, random)));
                                    Finder.find(this.myAgent, "distributor").forEach(msg::addReceiver);
                                    msg.setReplyByDate(new Date(System.currentTimeMillis() + 10000));
                                    addBehaviour(new FilmProposer(this.myAgent, msg));
                                }
                            }
                        }
                    }
                } catch (FIPAException ex) {
                    Logger.getLogger(TickerBehaviour.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        addBehaviour(new FilmNegociationProposer(this, MessageTemplate.MatchConversationId("Negociation")));

        // 2. Register the productor as a service in the yellow pages
        registerService(SERVICE, this);
    }

    public Productor getProductor() {
        return productor;
    }
}
