package fr.miage.aai.productor.strategy;


import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public abstract class IStrategy {

    public String film;
    public boolean exclusivity;
    public double priceProd = 0;
    public double offerReceived;
    public double offerPrevious = -1;

    public double threshold = 10000;
    public boolean agreement;
    public boolean rejection;
    public int step = 0;
    public int maxStep = 10;

    public abstract PriceProposalNegociationModel negociation(PriceProposalNegociationModel ppnm, boolean exclusivity, boolean sold);


}
