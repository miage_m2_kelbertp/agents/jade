package fr.miage.aai.productor.strategy;

import fr.miage.aai.utils.models.PriceProposalNegociationModel;

import java.util.ArrayList;

public class Strategy2 extends IStrategy {

    ArrayList<Double> lastProposals = new ArrayList<Double>();

    @Override
    public PriceProposalNegociationModel negociation(PriceProposalNegociationModel ppnm, boolean exclusivity, boolean sold) {

        // STORAGE OF OFFERS RECEIVED
        lastProposals.add(offerReceived);

        // IF THE FILM HAS ALREADY BEEN PURSHASED AS AN EXLUSIVE
        if(exclusivity){
            rejection = true;
            return ppnm;
        }

        // IF THE FILM HAS ALREADY BEEN PURCHASED ONCE
        if(sold && ppnm.getExclu() == true){
            ppnm.setExclu(false);
        }

        // IF STEP == 0
        if (offerPrevious <= 0){
            ppnm.setPrice(priceProd * 3);
            if (ppnm.getExclu()) {
                ppnm.setPrice(priceProd * 4);
            }
            offerPrevious = ppnm.getPrice();
            return ppnm;
        }

        // IF < SEUIL
        if(offerReceived < threshold){
            rejection = true;
            return ppnm;
        }

        if (offerReceived > offerPrevious) {
            agreement = true;
            return ppnm;
        }

        // TEST OF THE LAST 3 PROPOSALS
        if (lastProposals.size() > 3) {
            // IF THE LAST 3 PROPOSALS ARE DECREASING
            if (lastProposals.get(lastProposals.size() - 1) < lastProposals.get(lastProposals.size() - 2) && lastProposals.get(lastProposals.size() - 2) < lastProposals.get(lastProposals.size() - 3)) {
                if (offerReceived > threshold) {
                    agreement = true;
                    return ppnm;
                }
            }
        }

        // LAST STEP
        if(step >= maxStep){
            ppnm.setPrice(threshold * 1.1);
            return ppnm;
        }

        // STEP X
        if(ppnm.getExclu() == true){
            ppnm.setPrice(offerPrevious * 0.80);
            if(ppnm.getPrice() < threshold){
                rejection = true;
            }
            return ppnm;
        }

        ppnm.setPrice(offerPrevious * 0.75);
        if(ppnm.getPrice() < threshold){
            rejection = true;
        }
        return ppnm;

    }
}
