package fr.miage.aai.productor.strategy;

import fr.miage.aai.utils.models.PriceProposalNegociationModel;

public class Strategy1 extends IStrategy {
    @Override
    public PriceProposalNegociationModel negociation(PriceProposalNegociationModel ppnm, boolean exclusivity, boolean sold) {

        // IF THE FILM HAS ALREADY BEEN PURSHASED AS AN EXLUSIVE
        if(exclusivity){
            rejection = true;
            return ppnm;
        }

        // IF THE FILM HAS ALREADY BEEN PURCHASED ONCE
        if(sold && ppnm.getExclu() == true){
            ppnm.setExclu(false);
        }

        // IF STEP == 0
        if (offerPrevious <= 0){
            ppnm.setPrice(priceProd * 1.50);
            if (ppnm.getExclu()) {
                ppnm.setPrice(priceProd * 2);
            }
            offerPrevious = ppnm.getPrice();
            return ppnm;
        }

        // IF < SEUIL
        if(offerReceived < threshold){
            rejection = true;
            return ppnm;
        }

        if (offerReceived > offerPrevious) {
            agreement = true;
            return ppnm;
        }

        // STEP X
        if(ppnm.getExclu() == true){
            ppnm.setPrice(offerPrevious * 0.95);
            if(ppnm.getPrice() < threshold){
                rejection = true;
            }
            return ppnm;
        }

        ppnm.setPrice(offerPrevious * 0.9);
        if(ppnm.getPrice() < threshold){
            rejection = true;
        }
        return ppnm;

    }

}
