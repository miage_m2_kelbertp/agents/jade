package fr.miage.aai.productor.model;

public class Film {
    private String title;

    public Film(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}
