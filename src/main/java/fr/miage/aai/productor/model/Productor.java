package fr.miage.aai.productor.model;

import java.util.HashMap;
import java.util.HashSet;

public class Productor {
    String id; // id of the agent
    String name; // Warner Bros, Sony Pictures, etc.
    HashMap<String, Integer> films = new HashMap<>(); // films and their price
    HashSet<String> filmsExclusivity = new HashSet<>(); // films that are exclusive to this productor

    HashSet<String> filmsSold = new HashSet<>(); // films that have been sold
    public static HashSet<String> filmsAlreadyCreated = new HashSet<>();
    double budget = 100000.0; // budget of the productor

    public Productor(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public HashMap<String, Integer> getFilms() {
        return films;
    }

    public boolean addFilm(String film, int price) {
        if (films.containsKey(film)) {
            return false;
        }
        films.put(film, price);
        return true;
    }

    public boolean addFilmExclusivity(String film) {
        if (filmsExclusivity.contains(film)) {
            return false;
        }
        filmsExclusivity.add(film);
        return true;
    }

    public boolean addFilmSold(String film) {
        if (filmsSold.contains(film)) {
            return false;
        }
        filmsSold.add(film);
        return true;
    }

    public boolean checkFilmExclusivity(String film) {
        return filmsExclusivity.contains(film);
    }

    public boolean checkFilmSold(String film) {
        return filmsSold.contains(film);
    }

    public double getBudget() {
        return budget;
    }

    public void addBudget(double amount) {
        budget += amount;
    }
}
