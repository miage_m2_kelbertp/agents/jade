/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.miage.aai.ticker;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.DataStore;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author castagnos
 */
class DateProviderBehaviour extends CyclicBehaviour implements fr.miage.aai.parameters.Status {

    private final static MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);

    public DateProviderBehaviour(Agent myAgent) {
        super(myAgent);
    }

    @Override
    public void action() {
        ACLMessage timerequest = super.myAgent.receive(mt);
        if (timerequest != null) {
            ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
            answer.addReceiver(timerequest.getSender());
            DataStore ds = this.getDataStore();
            try {
                answer.setContentObject((LocalDate) ds.get(DATE));
                super.myAgent.send(answer);
            } catch (IOException ex) {
                Logger.getLogger(DateProviderBehaviour.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
