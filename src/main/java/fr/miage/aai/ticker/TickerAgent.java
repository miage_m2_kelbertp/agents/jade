/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.miage.aai.ticker;

import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import java.time.LocalDate;

/**
 *
 * @author castagnos
 */
public class TickerAgent extends Agent implements fr.miage.aai.parameters.Status {

    private LocalDate currentdate;

    @Override
    protected void setup() {
        currentdate = LocalDate.now();
        System.out.println("Starting date: "+currentdate.toString());

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(super.getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType("ticker");
        sd.setName("ticker");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            System.err.println(getLocalName()
                    + " registration with DF unsucceeded. Reason: " + e.getMessage());
            super.doDelete();
        }

        ParallelBehaviour pb = new ParallelBehaviour(ParallelBehaviour.WHEN_ALL);
        DataStore ds = new DataStore();
        ds.put(DATE, currentdate);
        pb.setDataStore(ds);
        DateProviderBehaviour dpb = new DateProviderBehaviour(this);
        dpb.setDataStore(ds);
        pb.addSubBehaviour(dpb);
        pb.addSubBehaviour(new TickerBehaviour(this, DEFAULT_WAIT) {
            @Override
            protected void onTick() {
                currentdate = currentdate.plusDays(1);
                ds.put(DATE, currentdate);
                System.out.println("******* " + currentdate.toString() + " *******");
            }
        });

        this.addBehaviour(pb);
        System.out.println("""
                               TickerAgent =====> TickerBehaviour
                                             || 
                                             ||=> DateProviderBehaviour
                           \n""");
    }
}
