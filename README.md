# Projet Agents et Assistants Intelligents

### Composition des groupes :


| **Groupe**            | **Nom Prénom** | **Nom Prénom**    | **Nom Prénom** |
| --------------------- | --------------- | ------------------ | --------------- |
| Distributeur          | Kelbert Paul    | Weingartner Olivia | Zanzi Léo      |
| Producteur            | Signer Tom      | Vignot Benjamin    | El Hssini Ali   |
| E-Réputation         | Mathieu Jean    | Noss Loïc         |                 |
| **Groupe Transverse** | Signer Tom      | Weingartner Olivia | Noss Loïc      |


### Lien de l'énoncé :

[Projet](https://arche.univ-lorraine.fr/pluginfile.php/160414/mod_resource/content/15/ProjetSID2022-23.pdf)

[Git](https://gitlab.com/miage_m2_kelbertp/agents/jade)

[](https://)

### Pour lancer le projet sur Intellij
![image.png](./image.png)
