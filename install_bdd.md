# Installation de la BDD :

### Installation sur le poste

La Base de données est sous MySQL. Afin de l'installer sur Docker, exécuter :

```bash
docker run --name mysql-agent -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql:oracle
```

# Importer les données

Pensez à créer la base de données "jade" !

```bash
docker exec -it mysql-agent mysql -uroot -proot

create database jade;

exit
```

Le fichier étant immense, il n'est pas possible de les exécuter de manière "manuelle". Il faut exécuter cette commande :

```bash
docker exec -i mysql-agent mysql -uroot -proot jade < script.sql
```

Si rien ne se passe, c'est normal ! Il faut juste attendre quelques secondes, jusqu'à la fin de l'exécution.


# Connexion

Un exemple de connexion est dans le fichier `fr.miage.aai.DatabaseConnexionExample`.


Il se sert d'un fichier de configuration `database.properties` dans le dossier `resources`. C'est à vous de le créer, avec cette structure :

```plaintext
URL=localhost:3306
DATABASE=jade
USER=root
PASSWORD=root
```
